module.exports = {
    paths: {
        src: {
            baseDir: 'public/wp-content/themes/ronaevents/assets/src/',
            css: 'scss/',
            js: 'js/',
            images: 'images/',
            fonts: 'fonts/',
        },
        dist: {
            baseDir: 'public/wp-content/themes/ronaevents/assets/dist/',
            css: 'css/',
            js: 'js/',
            images: 'images/',
            fonts: 'fonts/',
        },
    },

    browserSync: {
        notify: false,
        open: true,
        //proxy:              'http://www.agencecc.dev'
    },

    libsass: {
        errLogToConsole: false,
        sourceMap: true,
        sourceComments: true,
        precision: 10,
        outputStyle: 'expanded',
        imagePath: 'public/wp-content/themes/ronaevents/assets/src/images',
        includePaths: [
            './node_modules/susy/sass',
            './node_modules/normalize-scss/sass/normalize'
        ]
    },

    cssnano: {
        safe: true
    },

    sassLint: {
        configFile: '.sass-lint.yml'
    },

    uglify: {
        compress: {
            pure_funcs: ['console.log']
        }
    },

    eslint: {
        configFile: './.eslintrc.yml'
    },

    imagemin: {
        progressive: true,
        interlaced: true,
        optimizationLevel: 3
    },

    svgSprite: {
        mode: {
            css: false,
            inline: true,
            symbol: true
        }
    },

    browserify: {
        entries: ['./public/wp-content/themes/ronaevents/assets/src/js/app.js'],
        debug: true
    },

    iconSizes: [180, 167, 152, 120, 96, 32, 16]
}
