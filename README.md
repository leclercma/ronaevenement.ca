# Project Install

Really cool front-end setup

### Node version

use nvm 8.9.1

### Install NPM modules

`npm install`
`bower install`

### Run project for dev

`gulp serve`

### Compile project for production

`gulp build`

### To compile different icon sizes

First, install GraphicsMagick
`brew install GraphicsMagick`

Edit your bash profile
`nano ~/.bash_profile`

Add /usr/local/Cellar/graphicsmagick/1.3.21/bin to it
`export PATH=${PATH}:/usr/local/Cellar/graphicsmagick/1.3.21/bin`

Use `gulp icons` to generate icon set
