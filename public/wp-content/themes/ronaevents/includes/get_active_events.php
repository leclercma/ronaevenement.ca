<?php
    $events = new WP_Query(
        array(
            'post_type' => array('events'),
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key'     => 'active',
                    'value'   => 1,
                    'compare' => '=',
                ),
            )
        )
    );
?>