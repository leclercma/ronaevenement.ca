<?php
    // Get all the products for the specified event
    // TODO: Fix to accept multiple events per products

    $args = array(
        'post_type' => array('product'),
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key'     => 'events',
                'value'   => $event_product_tags, //$event_product_tags
                'compare' => 'LIKE',
            ),
        )
    );

    // if($is_category_page) {
    //     $args['tax_query'] = array(
    //         array(
    //             'taxonomy'      => 'product_cat',
    //             'field'         => 'term_id',
    //             'terms'         => get_queried_object()->term_id,
    //             'operator'      => 'IN'
    //         )
    //     );
    // }

    $posts = new WP_Query($args);
?>