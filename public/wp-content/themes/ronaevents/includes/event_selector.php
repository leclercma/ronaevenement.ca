<?php
$now = date('Y-m-d G:i:s');
$event_end_date_modifier = "-1 hours";
// If no event has been selected yet
if(!isset($_SESSION['event_id'])):
    include 'get_active_events.php'; ?>
    <div class="row justify-content-center">
        <div class="col-10">
            <div class="row">
            <?php while($events->have_posts()) : $events->the_post();
                $event_start_date = date(get_field('start_date'));
                $event_end_date = date(get_field('end_date'));
                
                $event_is_started = $event_start_date <= $now ? true : false;
                $event_has_ended = $event_is_started && ($now > $event_end_date) ? true : false; ?>
                <!-- If no event has been selected yet -->
                <div class="col col-sm-6 d-flex">
                    <div class="event">
                        <!-- <div class="event__icon">
                            <img width="100" src="<?= get_field('icone')['url'] ?>" alt="">
                        </div> -->
                        <h2 class="event__title"><?php the_title(); ?></h2>
                        <!-- <img width="100" src="<?= get_field('fournisseurs')['url'] ?>" alt=""> -->

                        <?php if(get_locale()=="fr_FR"): ?>
                            <?php setlocale(LC_ALL, 'fr_FR'); ?>
                            <p>
                                Du <?= utf8_encode(strftime("%A %d %B", strtotime($event_start_date))); ?> à <?= strftime("%Hh%M", strtotime($event_start_date)); ?><br>
                                au <?= utf8_encode(strftime("%A %d %B", strtotime($event_end_date))); ?> à <?= strftime("%Hh%M", strtotime($event_end_date . $event_end_date_modifier)); ?>
                            </p>
                        <?php else: ?>
                            <?php setlocale(LC_ALL, 'en_US'); ?>
                            <p>
                                From <?= strftime("%A %d %B at %Hh%M", strtotime($event_start_date)), "\n"; ?><br>
                                to <?= strftime("%A %d %B at %Hh%M", strtotime($event_end_date . $event_end_date_modifier)), "\n"; ?>
                            </p>
                        <?php endif; ?>


                        <a href="<?php the_permalink(); ?>" class="button"><?= __('Choisir cet événement', 'lang'); ?></a>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
        </div>
    </div>
    <?php wp_reset_postdata(); ?>
<?php else:
    // User has already selected an event
    $event_id = $_SESSION['event_id'];
    $event_title = get_title($event_id);
    $event_start_date = $_SESSION['event_start_date'];
    $event_end_date = $_SESSION['event_end_date'];
    $event_is_started = $event_start_date <= $now ? true : false;
    $event_has_ended = $event_is_started && ($now > $event_end_date) ? true : false; ?>
    <div class="row align-items-center justify-content-center">
        <div class="col">
            <div class="intro__event-logo">
                <img src="<?= get_field('icone', $event_id)['url'] ?>" alt="">
            </div>
            <div class="intro__info">
                <div class="intro__title"><?= $event_title ?></div>
                <div class="intro__date">
                    <?php if(get_locale()=="fr_FR"): ?>
                        <?php setlocale(LC_ALL, 'fr_FR'); ?>
                        <?= utf8_encode(strftime("%A %d %B", strtotime($event_start_date))); ?> à <?= strftime("%Hh%M", strtotime($event_start_date)); ?>
                    <?php else: ?>
                        <?php setlocale(LC_ALL, 'en_US'); ?>
                        <?= strftime("%A %d %B à %Hh%M", strtotime($event_end_date)), "\n"; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="intro__provider"><img src="<?= get_field('fournisseurs', $event_id)['url'] ?>" alt=""></div>
            <?php if (!$event_has_ended) : ?>
                <div class="intro__countdown">
                    <div class="countdown js-countdown" now="<?= date('Y-m-d G:i:s'); ?>" start="<?= $event_start_date; ?>" data-date="<?= ($event_is_started) ? date("Y-m-d\TH:i:s",strtotime($event_end_date . $event_end_date_modifier)) : date("Y-m-d\TH:i:s",strtotime($event_start_date)) ?>">
                        <div class="countdown__box countdown__days">
                            <div class="countdown__number">00</div>
                            <div class="countdown__text"><?= __('Jours', 'lang'); ?></div>
                        </div>
                        <div class="countdown__box countdown__hours">
                            <div class="countdown__number">00</div>
                            <div class="countdown__text"><?= __('Heures', 'lang'); ?></div>
                        </div>
                        <div class="countdown__box countdown__minutes">
                            <div class="countdown__number">00</div>
                            <div class="countdown__text"><?= __('Minutes', 'lang'); ?></div>
                        </div>
                        <div class="countdown__box countdown__seconds">
                            <div class="countdown__number">00</div>
                            <div class="countdown__text"><?= __('Secondes', 'lang'); ?></div>
                        </div>
                    </div>
                </div>
                <?php if ($event_is_started) : ?>
                    <div class="intro__text"><?= __('Avant la fin de l\'événement', 'lang'); ?></div>
                <?php else : ?>
                    <div class="intro__text"><?= __('Avant le début de l\'événement', 'lang'); ?></div>
                <?php endif; ?>
            <?php else : ?>
                <div class="intro__title"><?= __('Événement terminé', 'lang'); ?></div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

