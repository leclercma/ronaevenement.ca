<?php

    $is_admin = in_array('administrator',  wp_get_current_user()->roles);
    
    // Uncomment to force close store for non-admin users
    // if(!$is_admin) {
    //       wp_redirect(get_home_url() . '?message=no-event'); // temp fix
    // }
    
    $eventId = $_SESSION["event_id"];
    if(isset($eventId) && $eventId !== null) {
        $shop_is_opened = false;
        // Get the currently selected event id
        $current_event_id = $eventId;
        // Get the events in the database that match the event id and that are active
        $events = new WP_Query(
            array(
                'post_type' => array('events'),
                'post__in' => array($eventId),
                'meta_query' => array(
                    array(
                        'key'     => 'active',
                        'value'   => 1,
                        'compare' => '=',
                    ),
                )
            )
        );
        // Make sure the event is actually started and not ended.
        while($events->have_posts()) {
            $events->the_post();
            $now = date('Y-m-d H:i:s');
            $event_is_active = get_field('active');
            $event_start_date = date(get_field('start_date'));
            $event_end_date = date(get_field('end_date'));
            $event_product_tags = get_field('event_product_tags');
            $event_is_started = $event_start_date <= $now;
            $event_has_ended = $event_is_started && ($now > $event_end_date);
            if($event_is_active && $event_is_started && !$event_has_ended) {
                $shop_is_opened = true;
            }
  
        }
        $is_admin = in_array('administrator',  wp_get_current_user()->roles);
        if(!$shop_is_opened && !$is_admin) wp_redirect(get_home_url()  . '?message=no-event');
    } else {
        wp_redirect(get_home_url() . '?message=no-event');
    }

?>