
<?php if ( function_exists('yoast_breadcrumb') ): ?>
  <div class="breadcrumb">
    <div class="container">
      <div class="row align-items-center">
        <div class="col">
          <?php yoast_breadcrumb("","",true); ?>
        </div>
        <div class="col d-flex justify-content-end">
          <div class="d-flex align-items-end flex-column">
            <?php if(isset($_SESSION['event_id'])): ?>
              <?php
                $now = date('Y-m-d G:i:s');
                $event_start_date = $_SESSION['event_start_date'];
                $event_end_date = $_SESSION['event_end_date'];
                $event_end_date_modifier = "-1 hours";
                $event_is_started = $event_start_date <= $now ? true : false;
                $event_has_ended = $event_is_started && ($now > $event_end_date) ? true : false;
              ?>
              <?php if (!$event_has_ended && $event_is_started) : ?>
                <strong class="breadcrumb__event-name"><?= get_title($_SESSION['event_id']); ?></strong>
                <span
                  class="countdown countdown--small js-countdown"
                  data-date="<?= date("Y-m-d\TH:i:s",strtotime($event_end_date . $event_end_date_modifier)) ?>">
                  <div class="countdown__box countdown__days">
                      <div class="countdown__number">00</div>
                  </div>:
                  <div class="countdown__box countdown__hours">
                      <div class="countdown__number">00</div>
                  </div>:
                  <div class="countdown__box countdown__minutes">
                      <div class="countdown__number">00</div>
                  </div>:
                  <div class="countdown__box countdown__seconds">
                      <div class="countdown__number">00</div>
                  </div>
                </span>
              <?php elseif($event_has_ended) : ?>
                <span class="countdown-text"><?= __('Événement terminé', 'lang'); ?></span>
              <?php endif; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif;?>