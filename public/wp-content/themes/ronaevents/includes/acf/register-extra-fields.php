<?php

function wooc_extra_register_fields() {
		global $woocommerce;
		$checkout = $woocommerce->checkout();
		$fields = $checkout->get_checkout_fields( 'billing' );
		$field = $fields["nom_du_rep"];
	?>
		<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
			<label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
			<label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide billing_company_field">
			<label for="reg_billing_company"><?php _e( $fields["billing_company"]["label"], 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_company" id="reg_billing_company" value="<?php if ( ! empty( $_POST['billing_company'] ) ) esc_attr_e( $_POST['billing_company'] ); ?>" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide billing_company_field">
			<label for="billing_address_1"><?php _e( $fields["billing_address_1"]["label"], 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_address_1" id="billing_address_1" value="<?php if ( ! empty( $_POST['billing_address_1'] ) ) esc_attr_e( $_POST['billing_address_1'] ); ?>" />
		</p>
		<p class="form-row address-field form-row-first" id="billing_city_field" data-priority="50">
			<label for="billing_city" class=""><?php _e( 'City', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text " name="billing_city" id="billing_city" placeholder="" value="" autocomplete="address-level2">
		</p>
		<p class="form-row address-field form-row-last validate-postcode" id="billing_postcode_field" data-priority="60">
			<label for="billing_postcode" class=""><?php _e( 'Postal code', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text " name="billing_postcode" id="billing_postcode" placeholder="" value="" autocomplete="postal-code">
		</p>

		<p class="form-row address-field form-row-first woocommerce-validated incl_desc" id="billing_state_field" data-priority="50">
			<label for="billing_state" class=""><?php _e( 'Province', 'woocommerce' ); ?></label>
			<input type="text" class="input-text " name="billing_state" disabled id="billing_state" placeholder="" value="Quebec">
		</p>
		<p class="form-row address-field form-row-last woocommerce-validated incl_desc" id="billing_country_field" data-priority="50">
			<label for="billing_country" class=""><?php _e( 'Country', 'woocommerce' ); ?></label>
			<input type="text" class="input-text " name="billing_country" disabled id="billing_country" placeholder="" value="Canada">
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-first incl_desc">
			<label for="numero_de_compte"><?php _e( 'Account number', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text" placeholder="0000XXXXXX" maxlength="10" name="numero_de_compte" id="numero_de_compte" value="<?php if ( ! empty( $_POST['numero_de_compte'] ) ) esc_attr_e( $_POST['numero_de_compte'] ); ?>" />
			<span class="short_desc"><?php _e("See your entrepreneur card");?></span>
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-last incl_desc">
			<label for="reg_billing_phone"><?php _e( 'Phone', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php esc_attr_e( $_POST['billing_phone'] ); ?>" />
		</p>

		<p class="form-row address-field form-row-wide woocommerce-form-row--wide" id="nom_du_rep_field" data-priority="50">
			<label for="nom_du_rep" class=""><?php _e( $field["label"], 'woocommerce' ); ?></label>
			<select class="js-select2-custom" id="nom_du_rep" name="nom_du_rep">
			<?php
			foreach($field["options"] as $option){
				?><option value="<?php echo $option;?>"><?php echo $option;?></option><?php
			}
			?>
			</select>
		</p>

	<?php
	 }
	 add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );

	 function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
	 	if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
	 		$validation_errors->add( 'billing_first_name_error', __( 'First name is required!', 'woocommerce' ) );
	 	}

	 	if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
	 		$validation_errors->add( 'billing_last_name_error', __( 'Last name is required!', 'woocommerce' ) );
	 	}

	 	if ( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) ) {
	 		$validation_errors->add( 'billing_company_error', __( 'Company is required!', 'woocommerce' ) );
	 	}

	 	if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
	 		$validation_errors->add( 'billing_phone_error', __( 'Phone is required!', 'woocommerce' ) );
	 	}

	 	if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) ) {
	 		$validation_errors->add( 'billing_address_1_error', __( 'Address is required!', 'woocommerce' ) );
	 	}

	 	if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) ) {
	 		$validation_errors->add( 'billing_city_error', __( 'City is required!', 'woocommerce' ) );
	 	}

	 	if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) ) {
	 		$validation_errors->add( 'billing_postcode_error', __( 'Postal code is required!', 'woocommerce' ) );
	 	}

	 	if ( isset( $_POST['numero_de_compte'] ) && empty( $_POST['numero_de_compte'] ) ) {
	 		$validation_errors->add( 'numero_de_compte_error', __( 'Account number is required!', 'woocommerce' ) );
	 	}

	 	if ( isset( $_POST['nom_du_rep'] ) && empty( $_POST['nom_du_rep'] ) ) {
	 		$validation_errors->add( 'nom_du_rep_error', __( 'Representant is required!', 'woocommerce' ) );
	 	}

	 	return $validation_errors;
	 }
	 add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );

	 function wooc_save_extra_register_fields( $customer_id ) {
	 	if ( isset( $_POST['billing_phone'] ) ) {
	 		// Phone input filed which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
	 	}
	 	if ( isset( $_POST['billing_first_name'] ) ) {
	 		//First name field which is by default
	 		update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
	 		// First name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
	 	}
	 	if ( isset( $_POST['billing_last_name'] ) ) {
	 		// Last name field which is by default
	 		update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
	 	}
	 	if ( isset( $_POST['numero_de_compte'] ) ) {
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'numero_de_compte', sanitize_text_field( $_POST['numero_de_compte'] ) );
	 	}
	 	if ( isset( $_POST['billing_company'] ) ) {
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
	 	}
	 	if ( isset( $_POST['billing_address_1'] ) ) {
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
	 	}
	 	if ( isset( $_POST['billing_city'] ) ) {
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
	 	}
	 	if ( isset( $_POST['billing_postcode'] ) ) {
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
	 	}
	 	if ( isset( $_POST['billing_state'] ) ) {
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['billing_state'] ) );
	 	}
	 	if ( isset( $_POST['billing_country'] ) ) {
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'billing_country', sanitize_text_field( $_POST['billing_country'] ) );
	 	}

	 	if ( isset( $_POST['nom_du_rep'] ) ) {
	 		// Last name field which is used in WooCommerce
	 		update_user_meta( $customer_id, 'nom_du_rep', sanitize_text_field( $_POST['nom_du_rep'] ) );
	 	}

	 }
     add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

?>