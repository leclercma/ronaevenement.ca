<?php
    global $woocommerce;
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $sale = get_post_meta( get_the_ID(), '_sale_price', true);
    $product = wc_get_product( get_the_ID() );
    $is_variable = $product->is_type('variable');
?>

<a class="c-product" href="<?= get_permalink() ?>">
    <div class="c-product__image">
        <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>" />
    </div>
    <div class="c-product__provider"><?= get_field('provider_name'); ?></div>
    <h2 class="c-product__title"><?php the_title(); ?></h2>
    <?php
        if($is_variable):
            $min_price = $product->get_variation_price( 'min' );
    ?>
        <div class="c-product__price">
            <div>
                <?php echo $min_price; ?><sup>$</sup>
            </div>
        </div>
    <?php else: ?>
        <?php if($sale) : ?>
            <div class="c-product__price">
                <div>
                    <del>
                    <?php echo $price; ?>$</del> <?php echo $currency; echo $sale; ?><sup>$</sup>
                </div>
            </div>
        <?php elseif($price) : ?>
            <div class="c-product__price">
                <div>
                    <?php echo $price; ?><sup>$</sup>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if(get_field('discount')): ?>
        <div class="c-product__discount">
            <?= get_field('discount'); ?>
        </div>
    <?php endif; ?>
    <div class="c-product__button-container">
        <div class="button" >
            <?= __('Choix des options', 'lang'); ?>
        </div>
    </div>
    <!-- <div class="c-product__image-provider">
        <img class="c-product__image-provider" src="<?= get_field('provider'); ?>" />
    </div> -->
</a>
