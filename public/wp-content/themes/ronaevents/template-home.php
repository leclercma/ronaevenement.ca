<?php
/*
Template Name: Home
*/
?>
<?php
	if(isset($_GET['action']) && $_GET['action'] === 'remove-event') {
		unset($_SESSION["event_start_date"]);
		unset($_SESSION["event_end_date"]);
		unset($_SESSION["event_id"]);
	}
?>
<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

	<div class="intro" style="background-image: url(<?php assets(); ?>/images/hero_rona.jpg);">
		<div class="container">
			<?php include 'includes/event_selector.php'; ?>

			<?php if(!is_user_logged_in()): ?>
				<!-- Utilisateur pas connecté -->
				<a href="<?= url(PAGE_ACCOUNT); ?>" class="button button--big"><?= __('S\'inscrire / Se connecter', 'lang'); ?></a>
			<?php else:
				if(isset($_SESSION['event_id'])):
					$now = date('Y-m-d H:i:s');
					$event_start_date = $_SESSION['event_start_date'];
					$event_end_date = $_SESSION['event_end_date'];
					$event_is_started = $event_start_date <= $now ? true : false;
					$event_has_ended = $event_is_started && ($now > $event_end_date) ? true : false;
					if ($event_is_started && !$event_has_ended) : ?>
						<!-- Utilisateur connecté et événement en cours -->
						<a href="<?= url(PAGE_SHOP); ?>" class="button button--big"><?= __('Voir les produits', 'lang'); ?></a>
					<?php endif; ?>
				<?php else: ?>
					<div class="intro__title"><?= __('Aucun événement sélectionné', 'lang'); ?></div>
				<?php endif; ?>
			<?php endif; ?>

		</div>
	</div>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>