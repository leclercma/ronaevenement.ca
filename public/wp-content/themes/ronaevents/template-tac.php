<?php
/*
Template Name: Terms and conditions
*/
?>

<?php get_header(); ?>

<div class="container">

	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

        <h1><?php the_title(); ?></h1>

        <?php if(isset($_SESSION['event_id'])): ?>
            <?php the_field('legal', $_SESSION['event_id']); ?>
        <?php endif; ?>

		<?php the_content(); ?>

	<?php endwhile; ?>
	<?php endif; ?>

</div>

<?php get_footer(); ?>