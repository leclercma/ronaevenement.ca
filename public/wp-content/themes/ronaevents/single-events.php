<?php
    $event_is_active = get_field('active');
    $event_start_date = date(get_field('start_date'));
    $event_end_date = date(get_field('end_date'));
    $event_product_tags = get_field('event_product_tags');
    $event_is_started = $event_start_date <= $now ? true : false;
    $event_has_ended = $event_is_started && ($now > $event_end_date) ? true : false;
    if($event_is_active && !$event_has_ended) {
        $_SESSION["event_start_date"]=$event_start_date;
        $_SESSION["event_end_date"]=$event_end_date;
        $_SESSION["event_id"]=get_the_id();
        wp_redirect(get_home_url());
    }
?>
<?php get_header(); ?>

<div class="container">
    <h1><?php the_title(); ?></h1>
    <p><?= __('Désolé, vous ne pouvez plus participer à cet événement.', 'lang'); ?></p>
    <a href="<?= get_home_url(); ?>" class="button">Retourner à l'accueil</a>
</div>

<?php get_footer(); ?>