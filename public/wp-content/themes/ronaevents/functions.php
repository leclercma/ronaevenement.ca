<?php

    date_default_timezone_set('America/Montreal');

	session_start();

	/*****************************************************/
	/****************** Pages IDS  ***********************/
	/*****************************************************/

	CONST PAGE_TAC = 330;
	CONST PAGE_ACCOUNT = 12;
	CONST PAGE_SHOP= 9;
	CONST PAGE_CART= 10;

	/*****************************************************/
	/*************** Custom post types  ******************/
	/*****************************************************/

	require_once('includes/cpt/events.php');

	/*****************************************************/
	/***************** Custom fields  ********************/
	/*****************************************************/

	require_once('includes/acf/register-extra-fields.php');

	/*****************************************************/
	/***************** Disable coupons  ******************/
	/*****************************************************/

	function disable_coupon_field_on_cart( $enabled ) {
		if ( is_cart() ) {
			$enabled = false;
		}
		return $enabled;
	}
	add_filter( 'woocommerce_coupons_enabled', 'disable_coupon_field_on_cart' );


	/*****************************************************/
	/************* Yoast Seo theme support ***************/
	/*****************************************************/

	add_theme_support( 'yoast-seo-breadcrumbs' );


	/*****************************************************/
	/********** Woocommerce Template Support  ************/
	/*****************************************************/

	function mytheme_add_woocommerce_support() {
		add_theme_support( 'woocommerce' );
	}
	add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

	/*****************************************************/
	/********** Remove woocommerce breadcrums ************/
	/*****************************************************/

	remove_action( 'init', 'woocommerce_breadcrumb', 20 );
	remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20 );

	/*****************************************************/
	/************** Add Yoast SEO breadcrums *************/
	/*****************************************************/

	add_action( 'woocommerce_before_main_content','my_yoast_breadcrumb', 20, 0 );
	if (!function_exists('my_yoast_breadcrumb') ) {
		function my_yoast_breadcrumb() {}
	}

	/*****************************************************/
	/********* Remove related products output ************/
	/*****************************************************/

	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

	/*****************************************************/
	/************ Wordpress custom values  ***************/
	/*****************************************************/

	if (function_exists('add_theme_support'))
	{
	    add_theme_support('menus');
	    add_theme_support('post-thumbnails');
	    add_image_size('large', 700, '', true);
	    add_image_size('medium', 250, '', true);
	    add_image_size('small', 120, '', true);
	    add_image_size('xlarge', 1200, 800, false);
	    add_image_size('fullscreen', 1440, 900, true);
	}

	/*****************************************************/
	/********** Inject JS scripts in the head ************/
	/*****************************************************/

	function head_scripts() {
	    echo '<script>';
	   	echo 'var assets = "' . get_bloginfo("template_url") . '/assets/dist";';
	   	echo 'var ajaxurl = "' . admin_url() . 'admin-ajax.php";';
	   	echo 'var language = "' . ICL_LANGUAGE_CODE . '";' ;
	    echo '</script>';
	}
	add_action( 'wp_enqueue_scripts', 'head_scripts' );

	/*****************************************************/
	/********* Remove default links on images ************/
	/*****************************************************/

	function wpb_imagelink_setup() {
		$image_set = get_option( 'image_default_link_type' );

		if ($image_set !== 'none') {
			update_option('image_default_link_type', 'none');
		}
	}
	add_action('admin_init', 'wpb_imagelink_setup', 10);

	/*****************************************************/
	/************* Change Excerpt Length *****************/
	/*****************************************************/

	function new_excerpt_length($length) {
		return 100;
	}
	add_filter('excerpt_length', 'new_excerpt_length');

	/*****************************************************/
	/******** Add Link in Wordpress Admin Footer *********/
	/*****************************************************/

	function remove_footer_admin () {
		echo '';
	}

	/*****************************************************/
	/************ Hide Wordpress Login Errors ************/
	/*****************************************************/

	function no_wordpress_errors(){
  		return 'Something is wrong!';
	}
	add_filter( 'login_errors', 'no_wordpress_errors' );

	/*****************************************************/
	/************ Remove options from the menu ***********/
	/*****************************************************/

	function remove_menus(){
		remove_menu_page( 'jetpack' );                    //Jetpack*
		remove_menu_page( 'edit-comments.php' );          //Comments
		//remove_menu_page( 'index.php' );                //Dashboard
		//remove_menu_page( 'edit.php' );                 //Posts
		//remove_menu_page( 'upload.php' );               //Media
		//remove_menu_page( 'edit.php?post_type=page' );  //Pages
		//remove_menu_page( 'themes.php' );               //Appearance
		//remove_menu_page( 'plugins.php' );              //Plugins
		//remove_menu_page( 'users.php' );                //Users
		//remove_menu_page( 'tools.php' );                //Tools
		//remove_menu_page( 'options-general.php' );      //Settings
	}
	add_action( 'admin_menu', 'remove_menus' );

	/*****************************************************/
	/************ Get WPML translated URLS ***************/
	/*****************************************************/

	function url($id) {
		echo get_permalink(icl_object_id($id));
	}

	function get_url($id) {
		return get_permalink(icl_object_id($id));
	}

	/*****************************************************/
	/********* Get WPML translated Page title ************/
	/*****************************************************/

	function get_title($id) {
		return get_the_title(icl_object_id($id));
	}

	/*****************************************************/
	/************* Output WPML Language links ************/
	/*****************************************************/

	function language_switcher(){
		$html_switcher = "";
	    $languages = icl_get_languages('skip_missing=0&orderby=code');
	    if(!empty($languages)){

	        foreach($languages as $l){

	        	if($l['active']!=1){

	            if(!$l['active'])
	            	$html_switcher .= '<a class="header__lang-switcher" href="'.$l['url'].'">';

	            $html_switcher .= strtoupper($l['language_code']);

	            if(!$l['active'])
	            	$html_switcher .= '</a>';
	            }
	        }
	        echo $html_switcher;
	    }
	}

	/*****************************************************/
	/********** Return Absolute path to assets ***********/
	/*****************************************************/

	function assets() {
		echo get_bloginfo("template_url") . '/assets/dist';
	}

	function get_assets() {
		return get_bloginfo("template_url") . '/assets/dist';
	}

	/*****************************************************/
	/******************* Outputs SVG *********************/
	/*****************************************************/

	function svg($id, $class) {
		echo '<svg viewBox="0 0 1 1" class="'.$class.'"><use xlink:href="'.get_assets().'/images/symbol/svg/sprite.symbol.svg#svg--'.$id.'"></use></svg>';
	}

	/*****************************************************/
	/************ Returns page template Slug *************/
	/*****************************************************/

	function getPageTemplateSlug() {
		$template_name = get_page_template_slug();
		$template_name = str_replace('template-','', $template_name);
		$template_name = str_replace('.php','', $template_name);
		return $template_name;
	}

	/*****************************************************/
	/******** Make Yoast SEO at bottom of page ***********/
	/*****************************************************/

	function lower_wpseo_priority( $html ) {
	    return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'lower_wpseo_priority' );

	/*****************************************************/
	/********* Add custom fields to search  **************/
	/*****************************************************/

	function cf_search_join( $join ) {
	    global $wpdb;

	    if ( is_search() ) {
	        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	    }

	    return $join;
	}
	add_filter('posts_join', 'cf_search_join' );


	function cf_search_where( $where ) {
	    global $wpdb;

	    if ( is_search() ) {
	        $where = preg_replace(
	            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
	            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
	    }

	    return $where;
	}
	add_filter( 'posts_where', 'cf_search_where' );


	function cf_search_distinct( $where ) {
	    global $wpdb;

	    if ( is_search() ) {
	        return "DISTINCT";
	    }

	    return $where;
	}
	add_filter( 'posts_distinct', 'cf_search_distinct' );

?>