<footer class="footer">
  <div class="container">
    <div class="footer__need-help"><b><?= __('Des questions ?', 'lang'); ?></b> <?= __('Communiquer en temps réel avec un représentant par courriel au', 'lang'); ?> <a href="mailto:support@vip-pro.ca">support@vip-pro.ca</a></div>
    <a class="footer__item" href="<?php url(PAGE_TAC); ?>"><?= get_title(PAGE_TAC); ?></a>
    <?php if(isset($_SESSION['event_id'])): ?>
      <a class="footer__item" href="<?= get_home_url(); ?>?action=remove-event"><?= __('Changer d\'événement', 'lang'); ?></a>
    <?php endif; ?>
  </div>
</footer>
<div class="footer-copyright"><div class="container">© RONA <?= date("Y"); ?>, <?= __('Tous droits réservés', 'lang'); ?></div></div>

<?php wp_footer(); ?>

<script src="<?php assets(); ?>/js/bundle.js?v=3"></script>

</body>
</html>