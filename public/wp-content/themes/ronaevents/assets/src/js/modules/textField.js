import $ from 'jquery';

export default function () {

  $(".input-text").each(function() {
    const $this = $(this);
    
    if($this.val() !== "")
      $this.closest(".form-row").addClass("form-row--filled");
  });

  $(".input-text").on('input',function(e) {
    const $this = $(this);
    if($this.val() !== "")
      $this.closest(".form-row").addClass("form-row--filled");
    else
      $this.closest(".form-row").removeClass("form-row--filled");
  });

  $(".input-text").on('focus',function(e) {
    const $this = $(this);
    $this.closest(".form-row").addClass("form-row--filled");
  });

  $(".input-text").on('blur',function(e) {
    const $this = $(this);
    if($this.val() === "")
      $this.closest(".form-row").removeClass("form-row--filled");
  });

}
