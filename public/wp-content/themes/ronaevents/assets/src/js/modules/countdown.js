import $ from "jquery";

export default function () {
  $(".js-countdown").each(function () {
    var $this = $(this);
    // Set the date we're counting down to
    var countDownDate = new Date($(".js-countdown").attr("data-date")).getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {
      // Get todays date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Output the result in an element with id="demo"
      $this.find(".countdown__days .countdown__number").text(days >= 10 ? days : "0" + days);
      $this.find(".countdown__hours .countdown__number").text(hours >= 10 ? hours : "0" + hours);
      $this.find(".countdown__minutes .countdown__number").text(minutes >= 10 ? minutes : "0" + minutes);
      $this.find(".countdown__seconds .countdown__number").text(seconds >= 10 ? seconds : "0" + seconds);

      // If the count down is over, write some text
      if (distance <= 0) {
        clearInterval(x);
        $this.find(".countdown__days .countdown__number").text("00");
        $this.find(".countdown__hours .countdown__number").text("00");
        $this.find(".countdown__minutes .countdown__number").text("00");
        $this.find(".countdown__seconds .countdown__number").text("00");
        //window.location.href = "http://www.ronaevenement.ca/";
      } else {
        // Output the result in an element with id="demo"
        $this.find(".countdown__days .countdown__number").text(days >= 10 ? days : "0" + days);
        $this.find(".countdown__hours .countdown__number").text(hours >= 10 ? hours : "0" + hours);
        $this.find(".countdown__minutes .countdown__number").text(minutes >= 10 ? minutes : "0" + minutes);
        $this.find(".countdown__seconds .countdown__number").text(seconds >= 10 ? seconds : "0" + seconds);
      }
    }, 1000);
  });
}
