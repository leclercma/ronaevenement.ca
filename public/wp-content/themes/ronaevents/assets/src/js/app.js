import $ from 'jquery';
import scroller from './modules/scroller';
import countdown from './modules/countdown';
import textField from './modules/textField';
// import select2 from 'select2'; 
import selectWoo from './modules/selectWoo.min.js'; 

scroller();
countdown();
textField();

$(document).ready(function() {
  // select2($);
  selectWoo($);
  // $('#nom_du_rep').select2();
  $('form:not(.checkout) #nom_du_rep').selectWoo();
  $('.variations select').selectWoo();
  $('.variations select').on('select2:select', function (e) { 
    jQuery('.variations select').trigger("change");
  });

  $(".js-button-mobile").on("click", function() {
    $(this).toggleClass("active");
    $(".header__menu").slideToggle();
  });

  $('[name="quantity"]').each(function() {
    
  });

  $('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
  $('.quantity').each(function() {
    var inputContainer = jQuery(this),
      input = inputContainer.find('input[type="number"]'),
      btnUp = inputContainer.find('.quantity-up'),
      btnDown = inputContainer.find('.quantity-down'),
      min = input.attr('min'),
      max = input.attr('max'),
      step = parseFloat(input.attr('step')) || 1;

    btnUp.click(function() {
      var oldValue = parseFloat(input.val());

      var newVal = oldValue + step;

      inputContainer.find("input").val(newVal);
      inputContainer.find("input").trigger("change");
    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue <= min) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue - step;
      }
      inputContainer.find("input").val(newVal);
      inputContainer.find("input").trigger("change");
    });

  });
});