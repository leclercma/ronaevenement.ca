<?php
    $is_category_page = is_product_category();
    include 'includes/requires_event.php';
    include 'includes/get_products.php';
?>

<?php get_header(); ?>

<div class="shop">
    <div class="container">
        <div class="shop-categories">
            <div class="shop-categories__title"><?= __('Catégories', 'lang'); ?>:</div>
            <div class="row justify-content-center">
                <div class="col-6 col-sm-4 col-md-3 col-lg-2 d-flex">
                    <a href="<?= get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="shop-categories__item shop-categories__item <?php if(!$is_category_page) : ?> active <?php endif; ?>">
                        <?= __('Voir tout', 'lang'); ?>
                    </a>
                </div>
                <?php
                    $categories = [];
                    while($posts->have_posts()) :
                        $posts->the_post();
                        $terms = get_the_terms( $post->ID, 'product_cat' );
                        foreach ($terms as $term) {
                            $c = get_term_by( 'id', $term->term_id, 'product_cat' );
                            
                            $category = [
                                "slug" => $c->slug,
                                "name" => $c->name,
                                "url" => get_term_link( $term->term_id, 'product_cat' )
                            ];
                            $categories[$term->term_id] = $category;
                        }
                    endwhile;
                ?>
                <?php foreach($categories as $category):?>
                    <?php //var_dump($category); ?>
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2 d-flex">
                        <a href="<?= $category['url']; ?>" class="shop-categories__item shop-categories__item <?php if($is_category_page && get_queried_object()->slug === $category['slug']) : ?> active <?php endif; ?>">
                            <?= $category['name']; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php // var_dump(get_queried_object()->slug); ?>
        <?php if($is_category_page): ?>
            <div class="row">
                <h1><?= single_term_title(); ?></h1>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col">
                <a href="" class="shop-category"></a>
            </div>
        </div>
        <div class="row">
            <?php
                while($posts->have_posts()) :
                    $posts->the_post();
                    if($is_category_page) {
                        $containsSelectedCategory = false;
                        $terms = get_the_terms( $post->ID, 'product_cat' );
                        foreach ($terms as $term) {
                            if(get_queried_object()->term_id == $term->term_id) {
                                $containsSelectedCategory = true;
                                break;
                            }
                        }
                    }
                    if($containsSelectedCategory || !$is_category_page):
            ?>
                        <div class="col-xs-12 col-md-6 col-lg-4 col-xl-3 d-flex">
                            <?php include 'includes/product_preview.php'; ?>
                        </div>
                    <?php endif; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
