<?php if(get_locale()=="fr_FR"): ?>
    <?php setlocale(LC_ALL, 'fr_FR'); ?>
    Publié <?php echo strftime("%A le %d %B", get_the_time('U')), "\n"; ?>
<?php else: ?>
    <?php setlocale(LC_ALL, 'en_US'); ?>
    Posted on <?php echo strftime("%A, %B %C", get_the_time('U')), "\n"; ?>
<?php endif; ?>