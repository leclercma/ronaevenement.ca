<!DOCTYPE html>
<html lang="<?php echo get_locale(); ?>">
<head>
	<title><?php wp_title(); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="icon" type="image/png" href="<?php assets(); ?>/images/icons/icon-16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php assets(); ?>/images/icons/icon-32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php assets(); ?>/images/icons/icon-96.png" sizes="96x96">
	<link rel="apple-touch-icon" href="<?php assets(); ?>/images/icons/icon-120.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php assets(); ?>/images/icons/icon-180.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php assets(); ?>/images/icons/icon-152.png">
	<link rel="apple-touch-icon" sizes="167x167" href="<?php assets(); ?>/images/icons/icon-167.png">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <?php wp_head(); ?>
	<link rel="stylesheet" href="<?php assets(); ?>/css/main.css">
</head>

<body>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-45080611-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-45080611-2');
</script>

<?php if(isset($_GET['message']) && $_GET['message'] === 'no-event'): ?>
	<div class="c-header-message">
		<?= __('La boutique n\'est présentement pas ouverte. Veuillez attendre le début de l\'événement.', 'lang'); ?>
	</div>
<?php endif; ?>

<header class="header">
	<div class="container">
		<div class="row">
			<div class="col d-flex align-items-center">
				<button class="header__button-mobile js-button-mobile d-inline-block d-md-none">
					<div></div>
					<div></div>
					<div></div>
				</button>
				<a class="header__logo" href="<?= get_home_url(); ?>">
					<img src="<?php assets(); ?>/images/logo-<?= substr(get_locale(), 0, 2); ?>.png" alt="">
				</a>
			</div>
			<div class="header__menu align-self-end flex-md-row">

				<a class="header__menu__item" href="<?= get_home_url(); ?>">
					<?= __('Accueil', 'lang'); ?>
				</a>
				<?php if(isset($_SESSION['event_id'])): ?>
					<a class="header__menu__item" href="<?= url(PAGE_SHOP); ?>">
						<?= __('Produits', 'lang'); ?>
					</a>
				<?php endif; ?>
				<?php if(!is_user_logged_in()): ?>
					<a class="header__menu__item" href="<?= url(PAGE_ACCOUNT); ?>">
						<?= __('Se connecter', 'lang'); ?>
					</a>
					<a class="header__menu__item" href="<?= url(PAGE_ACCOUNT); ?>">
						<?= __('S\'enregistrer', 'lang'); ?>
					</a>
				<?php endif; ?>
				<?php if(is_user_logged_in()): ?>
					<a class="header__menu__item" href="<?= url(PAGE_ACCOUNT); ?>">
						<?= __('Mon compte', 'lang'); ?>
					</a>
					<a class="header__menu__item" href="<?= wp_logout_url(get_home_url()); ?>"><?= __('Se déconnecter', 'lang'); ?></a>
				<?php endif; ?>
			</div>
			<div class="col">
				<div class="header__menu-right">
					<?php //language_switcher(); ?>
					<a href="<?= wc_get_cart_url(); ?>"class="header__cart">
						<?php include("assets/dist/images/svg/icon-cart.svg") ?>
						<?php
							$cart_items_count = WC()->cart->get_cart_contents_count();
							if($cart_items_count > 0):
						?>
							<div class="header__cart__number">
								<?= $cart_items_count; ?>
							</div>
						<?php endif; ?>
					</a>
				</div>
			</div>
		</div>
	</div>
</header>

<?php if(!is_404()): ?>
	<?php include 'includes/breadcrumb.php'; ?>
<?php endif; ?>