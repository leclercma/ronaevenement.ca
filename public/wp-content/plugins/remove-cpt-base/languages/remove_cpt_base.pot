#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Remove CPT base\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-21 13:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#. Author URI of the plugin
msgid "https://kubiq.sk"
msgstr ""

#. Author of the plugin
msgid "KubiQ"
msgstr ""

#: remove-cpt-base.php:62
msgid "Remove base slug from url for these custom post types:"
msgstr ""

#. Name of the plugin
#: remove-cpt-base.php:49 remove-cpt-base.php:50
msgid "Remove CPT base"
msgstr ""

#. Description of the plugin
msgid "Remove custom post type base slug from permalink url"
msgstr ""
