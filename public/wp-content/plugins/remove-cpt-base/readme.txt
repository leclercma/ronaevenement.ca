=== Remove CPT base ===
Contributors: kubiq
Donate link: https://kubiq.sk
Tags: permalink, custom post type, base, slug, remove
Requires at least: 3.0
Tested up to: 5.0
Stable tag: 3.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Remove custom post type base slug from permalink url

== Description ==

Remove custom post type base slug from permalink url

<ul>
	<li>auto-prevent slug duplicates</li>
	<li>possibility to select only specific custom post types</li>
	<li>auto redirect old slugs to no-base slugs</li>
</ul>

== Installation ==

1. Upload `remove-cpt-base` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 3.3 =
* change HTTP code from 404 to 200

= 3.2 =
* fix for query strings

= 3.1 =
* add custom endpoint rewrites support

= 3.0 =
* stop using complicated 'pre_get_posts' and handle 404 instead

= 2.3 =
* tested on WP 5.0

= 2.2 =
* fix 404

= 2.1 =
* fix redirect loop in WPML and WooCommerce

= 2.0 =
* stop using .htaccess rules

= 1.2 =
* auto init after permalinks updated

= 1.1 =
* add uninstall hook
* add duplicate slug check
* minor updates

= 1.0 =
* First version