<?php
/*
	Plugin Name: Remove CPT base
	Description: Remove custom post type base slug from permalink url
	Version: 3.3
	Author: KubiQ
	Author URI: https://kubiq.sk
	Text Domain: remove_cpt_base
	Domain Path: /languages
*/

class remove_cpt_base{
	var $plugin_admin_page;
	var $rcptb_selected;
	static $instance = null;

	static public function init(){
		if( self::$instance == null ){
			self::$instance = new self();
		}
		return self::$instance;
	}

	function __construct(){
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );

		$this->rcptb_selected = get_option( 'rcptb_selected' );
		if( ! is_array( $this->rcptb_selected ) ) $this->rcptb_selected = array();

		add_action( 'admin_menu', array( $this, 'plugin_menu_link' ) );
		add_filter( 'post_type_link', array( $this, 'remove_slug' ), 10, 3 );
		add_filter( 'wp_unique_post_slug', array( $this, 'fix_slug' ), 100, 6 );
		add_action( 'template_redirect', array( $this, 'handle_404' ), 0 );
		add_action( 'template_redirect', array( $this, 'auto_redirect_old' ), 1 );
	}

	function plugins_loaded(){
		load_plugin_textdomain( 'remove_cpt_base', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
	}
	
	function filter_plugin_actions( $links, $file ){
		$settings_link = '<a href="options-general.php?page=' . basename( __FILE__ ) . '">' . __('Settings') . '</a>';
		array_unshift( $links, $settings_link );
		return $links;
	}
	
	function plugin_menu_link(){
		$this->plugin_admin_page = add_submenu_page(
			'options-general.php',
			__( 'Remove CPT base', 'remove_cpt_base' ),
			__( 'Remove CPT base', 'remove_cpt_base' ),
			'manage_options',
			basename( __FILE__ ),
			array( $this, 'admin_options_page' )
		);
		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'filter_plugin_actions' ), 10, 2 );
	}

	function admin_options_page(){
		if ( get_current_screen()->id != $this->plugin_admin_page ) return;
		global $wp_post_types; ?>
		<div class="wrap">
			<h2><?php _e( 'Remove base slug from url for these custom post types:', 'remove_cpt_base' ) ?></h2><?php
			if( isset( $_POST['rcptb_selected_sent'] ) ){
				if( isset( $_POST['rcptb_selected'] ) && is_array( $_POST['rcptb_selected'] ) ){
					$this->rcptb_selected = array_map( 'esc_attr', (array)$_POST['rcptb_selected'] );
				}else{
					$this->rcptb_selected = array();
				}
				update_option( 'rcptb_selected', $this->rcptb_selected, 'no' );
				echo '<div class="below-h2 updated"><p>'.__( 'Settings saved.' ).'</p></div>';
				flush_rewrite_rules();
			} ?>
			<br>
			<form method="POST" action="">
				<input type="hidden" name="rcptb_selected_sent" value="1"><?php
				foreach( $wp_post_types as $type => $custom_post ){
					if( $custom_post->_builtin == false ){ ?>
						<div>
							<label>
								<input type="checkbox" name="rcptb_selected[]" value="<?php echo $custom_post->name ?>" <?php echo in_array( $custom_post->name, $this->rcptb_selected ) ? 'checked' : '' ?>>
								<?php echo $custom_post->label ?> (<?php echo $custom_post->name ?>)
							</label>
						</div><?php
					}
				} ?>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save') ?>">
				</p>
			</form>
		</div><?php
	}

	function fix_slug( $slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug ){
		global $wpdb;

		$post_types_no_conflict = array_merge( $this->rcptb_selected, array( 'post', 'page', 'attachment' ) );

		$post_types_sql = function() use ( $post_types_no_conflict ){
			$ret = '';
			foreach( $post_types_no_conflict as $post_type ){
				$ret .= "'{$post_type}', ";
			}
			return substr( $ret, 0, -2 );
		};
		
		// If we are creating the unique slug for an affected post type
		if( in_array( $post_type, $post_types_no_conflict ) ){
			$unique = false;
			// While we haven't found a unique link
			while( $unique !== true ){
				$post_name_check = $wpdb->get_var(
					$wpdb->prepare(
						"SELECT post_name FROM $wpdb->posts WHERE post_name = %s AND post_type IN (" . $post_types_sql() . ") AND ID != %d LIMIT 1",
						$slug,
						$post_ID
					)
				);
				// Not unique? Append some numbers and let's try again!
				if( $post_name_check ){
					$slug = "{$slug}-2";
				}else{
					//If unique, we're done
					$unique = true;
				}
			}
			return $slug;
		}else{
			return $slug;
		}
	}

	function remove_slug( $permalink, $post, $leavename ){
		global $wp_post_types;
		foreach( $wp_post_types as $type => $custom_post ){
			if( $custom_post->_builtin == false && $type == $post->post_type && in_array( $custom_post->name, $this->rcptb_selected ) ){
				$custom_post->rewrite['slug'] = trim( $custom_post->rewrite['slug'], '/' );
				$permalink = str_replace( '/' . $custom_post->rewrite['slug'] . '/', '/', $permalink );
			}
		}
		return $permalink;
	}

	function handle_404(){
		global $wp_query, $post, $wpdb;
		if( $wp_query->is_404 == 1 ){
			$post_name = '';
			if( isset( $wp_query->query['name'] ) && $wp_query->query['name'] ){
				$post_name = $wp_query->query['name'];
			}
			if( isset( $wp_query->query['category_name'] ) && $wp_query->query['category_name'] ){
				$post_name = $wp_query->query['category_name'];
			}
			if( $post_name ){
				$post_type = $wpdb->get_var(
					$wpdb->prepare(
						"SELECT post_type FROM {$wpdb->posts} WHERE post_name = %s",
						$post_name
					)
				);
				if( in_array( $post_type, $this->rcptb_selected ) ){
					status_header(200);
					$args = array(
						'post_type' => $post_type,
						$post_type => $post_name,
						'name' => $post_name
					);

					$url = strtok( $_SERVER['REQUEST_URI'], '?' );
					$url = explode( '/' . $post_name . '/', $url );
					$url = end( $url );
					if( $url ){
						$url = explode( '/', $url );
						$endpoint = array_shift( $url );
						$args[ $endpoint ] = implode( '/', $url );
					}else{
						$args['page'] = null;
					}

					$wp_query = new WP_Query( $args );
					while( $wp_query->have_posts() ){
						$wp_query->the_post();
					}
				}
			}
		}
	}

	function auto_redirect_old(){
		global $post;
		if( is_single() && is_object( $post ) && in_array( $post->post_type, $this->rcptb_selected ) ){
			$new_url = get_permalink();
			$real_url = site_url() . $_SERVER['REQUEST_URI'];
			if( strstr( $real_url, $new_url ) == false ){
				remove_filter( 'post_type_link', array( $this, 'remove_slug' ), 10 );
				$old_url = get_permalink();
				add_filter( 'post_type_link', array( $this, 'remove_slug' ), 10, 3 );
				$fixed_url = str_replace( $old_url, $new_url, $real_url );
				wp_redirect( $fixed_url, 301 );
			}
		}
	}
}

function rcptb_remove_plugin_options(){
	delete_option('rcptb_selected');
}

add_action( 'init', array( 'remove_cpt_base', 'init' ), 99 );
register_activation_hook( __FILE__, 'flush_rewrite_rules' );
register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
register_uninstall_hook( __FILE__, 'rcptb_remove_plugin_options' );