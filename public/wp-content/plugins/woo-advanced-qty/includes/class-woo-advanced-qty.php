<?php
	/**
	 * The file that defines the core plugin class for woo-advanced-qty plugin
	 *
	 * @link       http://morningtrain.dk
	 * @since      1.0.0
	 *
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/includes
	 */

	/**
	 * The core plugin class.
	 *
	 * This is used to define internationalization, admin-specific hooks, and
	 * public-facing site hooks.
	 *
	 * Also maintains the unique identifier of this plugin as well as the current
	 * version of the plugin.
	 *
	 * @since      1.0.0
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/includes
	 * @author     Martin Schadegg Rasch Jensen <ms@morningtrain.dk>
	 */
	if(!class_exists('Woo_Advanced_Qty')) {
		class Woo_Advanced_QTY {

			/**
			 * The loader that's responsible for maintaining and registering all hooks that power
			 * the plugin.
			 *
			 * @since    1.0.0
			 * @access   protected
			 * @var      Woo_Advanced_QTY_Loader $loader Maintains and registers all hooks for the plugin.
			 */
			protected $loader;

			/**
			 * The unique identifier of this plugin.
			 *
			 * @since    1.0.0
			 * @access   protected
			 * @var      string $plugin_name The string used to uniquely identify this plugin.
			 */
			protected $plugin_name;

			/**
			 * The current version of the plugin.
			 *
			 * @since    1.0.0
			 * @access   protected
			 * @var      string $version The current version of the plugin.
			 */
			protected $version;

			/**
			 * Define the core functionality of the plugin.
			 *
			 * Set the plugin name and the plugin version that can be used throughout the plugin.
			 * Load the dependencies, define the locale, and set the hooks for the admin area and
			 * the public-facing side of the site.
			 *
			 * @since    1.0.0
			 */
			public function __construct() {
				$this->setPluginInfo();
				$this->load_dependencies();
				$this->set_locale();
				$this->define_admin_hooks();
				$this->define_public_hooks();
			}

			/**
			 * Set the Plugin info
			 *
			 * @since 2.4.0
			 */
			private function setPluginInfo() {
				$plugin = get_file_data(WP_PLUGIN_DIR . '/woo-advanced-qty/woo-advanced-qty.php', array(
					'Version'    => 'Version',
					'TextDomain' => 'Text Domain'
				), 'plugin');

				$this->plugin_name = $plugin['TextDomain'];
				$this->version = $plugin['Version'];
			}

			/**
			 * Load the required dependencies for this plugin.
			 *
			 * Create an instance of the loader which will be used to register the hooks
			 * with WordPress.
			 *
			 * @since    1.0.0
			 * @access   private
			 */
			private function load_dependencies() {
				/**
				 * The class responsible for orchestrating the actions and filters of the
				 * core plugin.
				 */
				require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-woo-advanced-qty-loader.php';
				$this->loader = new Woo_Advanced_QTY_Loader();

				/**
				 * The class responsible for defining internationalization functionality
				 * of the plugin.
				 */
				require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-woo-advanced-qty-i18n.php';

				/**
				 * The class responsible for defining all actions that occur in the admin area.
				 */
				require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-woo-advanced-qty-admin.php';

				/**
				 * The class responsible for defining all actions that occur in the public-facing
				 * side of the site.
				 */
				require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-woo-advanced-qty-public.php';

				/**
				 * The class which checks for plugin updates
				 */
				require_once plugin_dir_path(dirname(__FILE__)) . 'includes/morningtrain_updateChecker.class.php';
				$updater = new morningtrain_updateChecker();
				$updater->initialize($this->plugin_name);
			}

			/**
			 * Define the locale for this plugin for internationalization.
			 *
			 * @since    1.0.0
			 * @access   private
			 */
			private function set_locale() {
				$plugin_i18n = new Woo_Advanced_QTY_i18n();
				$plugin_i18n->set_domain($this->plugin_name);

				$this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
			}

			/**
			 * Register all of the hooks related to the admin area functionality
			 * of the plugin.
			 *
			 * @since    1.0.0
			 * @access   private
			 */
			private function define_admin_hooks() {
				$plugin_admin = new Woo_Advanced_QTY_Admin($this->plugin_name, $this->version);

				// Add settings tab for products
				$this->loader->add_filter('woocommerce_product_data_tabs', $plugin_admin, 'add_product_tab', 10, 1);
				$this->loader->add_action('woocommerce_product_data_panels', $plugin_admin, 'product_options');
				$this->loader->add_action('save_post', $plugin_admin, 'save_product_options', 10, 2);
				$this->loader->add_action('product_cat_add_form_fields', $plugin_admin, 'category_options_add', 99);
				$this->loader->add_action('product_cat_edit_form_fields', $plugin_admin, 'category_options_edit', 99);
				$this->loader->add_action('created_product_cat', $plugin_admin, 'save_category_options', 10, 2);
				$this->loader->add_action('edited_product_cat', $plugin_admin, 'save_category_options', 10, 2);
				//$this->loader->add_filter('woocommerce_products_general_settings', $plugin_admin, 'general_options');
				$this->loader->add_action('woocommerce_product_options_inventory_product_data', $plugin_admin, 'variations_options', 10);
				$this->loader->add_action('woocommerce_update_options_products', $plugin_admin, 'modify_global_settings');
				$this->loader->add_action('woocommerce_settings_tabs_array', $plugin_admin, 'addSettingsTab', 99);
				$this->loader->add_action('woocommerce_settings_tabs_advanced_quantity', $plugin_admin, 'getSettings', 99);
				$this->loader->add_action('woocommerce_update_options_advanced_quantity', $plugin_admin, 'updateSettings');

				// Only display dependency errors on plugins page
				if(!empty($GLOBALS['pagenow']) && $GLOBALS['pagenow'] === 'plugins.php') {
					$this->loader->add_action('admin_notices', $plugin_admin, 'checkAdminNotices');
				}

				$this->loader->add_filter('plugin_action_links_' . $this->plugin_name . '/' . $this->plugin_name . '.php', $plugin_admin, 'displayPluginSettingsLink');
			}

			/**
			 * Register all of the hooks related to the public-facing functionality
			 * of the plugin.
			 *
			 * @since    1.0.0
			 * @access   private
			 */
			private function define_public_hooks() {
				$plugin_public = new Woo_Advanced_QTY_Public($this->plugin_name, $this->version);

				$this->loader->add_filter('woocommerce_quantity_input_args', $plugin_public, 'qty_input_args', 100, 2);
				$this->loader->add_filter('woocommerce_available_variation', $plugin_public, 'available_variation', 100, 3);
				$this->loader->add_filter('woocommerce_order_amount_item_total', $plugin_public, 'order_amount_item_total', 10, 5);
				$this->loader->add_action('woocommerce_after_cart_item_quantity_update', $plugin_public, 'after_quantity_update', 10, 3);
				$this->loader->add_filter('woocommerce_add_cart_item', $plugin_public, 'add_to_cart_item_quantity', 10, 2);
				$this->loader->add_filter('woocommerce_is_sold_individually', $plugin_public, 'is_sold_individually', 10, 2);
				$this->loader->add_filter('woocommerce_add_to_cart_sold_individually_quantity', $plugin_public, 'add_to_cart_sold_individually_quantity', 10, 5);
				$this->loader->add_filter('woocommerce_cart_item_quantity', $plugin_public, 'cart_item_quantity', 10, 2);
				$this->loader->add_filter('woocommerce_paypal_args', $plugin_public, 'paypal_args', 10, 2);
				$this->loader->add_filter('woocommerce_add_to_cart_validation', $plugin_public, 'add_to_cart_qty_validation', 10, 6);
				$this->loader->add_filter('woocommerce_quantity_input_step', $plugin_public, 'order_items_quantity_step', 10, 2);
				$this->loader->remove_filter('woocommerce_stock_amount', NULL, 'intval');
				$this->loader->add_filter('woocommerce_stock_amount', NULL, 'floatval');
				$this->loader->add_filter('woocommerce_loop_add_to_cart_link', $plugin_public, 'archive_add_to_cart_button', 10, 2);
				$this->loader->add_filter('woocommerce_product_add_to_cart_url', $plugin_public, 'add_to_cart_url', 10, 2);
				// Add price suffix
				$this->loader->add_filter('woocommerce_get_price_suffix', $plugin_public, 'add_price_suffix', 10, 2);
				// Add quantity suffix
				$this->loader->add_action('woocommerce_after_template_part', $plugin_public, 'add_quantity_suffix', 10, 4);
				$this->loader->add_filter('wc_get_template', $plugin_public, 'woocommerce_locate_template', 10, 5);
				$this->loader->add_action('waq_get_input_type', $plugin_public, 'add_specific_input_picker', 10, 2);
				$this->loader->add_action('woocommerce_before_variations_form', $plugin_public, 'allow_individually_on_variations_product', 10, 2);
				$this->loader->add_action('woocommerce_before_single_product', $plugin_public, 'add_extra_info_for_input', 10, 0);
				$this->loader->add_filter('woocommerce_order_items_meta_display', $plugin_public, 'orderItemsMetaDisplay', 10, 2);
				$this->loader->add_filter('woocommerce_order_again_cart_item_data', $plugin_public, 'addInfoForOrderAgain', 10, 3);
				$this->loader->add_filter('woocommerce_paypal_line_item', $plugin_public, 'paypalLineItem', 10, 4);
				$this->loader->add_filter('woocommerce_order_amount_item_subtotal', $plugin_public, 'amountItemSubTotal', 10, 5);
				$this->loader->add_filter('woocommerce_update_cart_validation', $plugin_public, 'update_cart_validation', 10, 4);
				$this->loader->add_filter('woocommerce_cart_item_quantity', $plugin_public, 'cartItemQuantity', 10, 3);
				$this->loader->add_filter('woocommerce_cart_item_price', $plugin_public, 'cartItemPrice', 10, 3);
				//$this->loader->add_filter('woocommerce_order_amount_item_total', $plugin_public, 'changeOrderAmountItemTotal', 10, 5);
				$this->loader->add_filter('woocommerce_checkout_cart_item_quantity', $plugin_public, 'addQuantitySuffxToOrderReview', 10, 2);
				$this->loader->add_filter('woocommerce_order_item_quantity_html', $plugin_public, 'addQuantitySuffxToOrderComplete', 10, 2);
				$this->loader->add_filter('woocommerce_email_order_item_quantity', $plugin_public, 'addQuantitySuffxToOrderComplete', 10, 2);
				$this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueueIfSet');
			}

			/**
			 * Run the loader to execute all of the hooks with WordPress.
			 *
			 * @since 1.0.0
			 * @access public
			 */
			public function run() {
				$this->loader->run();
			}
		}
	}