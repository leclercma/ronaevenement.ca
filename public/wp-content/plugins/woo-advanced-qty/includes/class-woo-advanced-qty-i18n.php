<?php
	/**
	 * Define the internationalization functionality
	 *
	 * Loads and defines the internationalization files for this plugin
	 * so that it is ready for translation.
	 *
	 * @link       http://morningtrain.dk
	 * @since      1.0.0
	 *
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/includes
	 */

	/**
	 * Define the internationalization functionality.
	 *
	 * Loads and defines the internationalization files for this plugin
	 * so that it is ready for translation.
	 *
	 * @since      1.0.0
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/includes
	 * @author     Martin Schadegg Rasch Jensen <ms@morningtrain.dk>
	 */
	if(!class_exists('Woo_Advanced_Qty_i18n')) {
		class Woo_Advanced_QTY_i18n {

			/**
			 * The domain specified for this plugin.
			 *
			 * @since    1.0.0
			 * @access   private
			 * @var      string $domain The domain identifier for this plugin.
			 */
			private $domain;

			/**
			 * Load the plugin text domain for translation.
			 *
			 * @since    1.0.0
			 */
			public function load_plugin_textdomain() {
				load_plugin_textdomain($this->domain, FALSE, dirname(plugin_basename(dirname(__FILE__))) . '/languages');
			}

			/**
			 * Set the domain equal to that of the specified domain.
			 *
			 * @since    1.0.0
			 *
			 * @param    string $domain The domain that represents the locale of this plugin.
			 */
			public function set_domain($domain) {
				$this->domain = $domain;
			}
		}
	}
