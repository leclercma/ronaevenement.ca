<?php
	/**
	 * Fired during plugin activation.
	 *
	 * @link       http://morningtrain.dk
	 * @since      2.4.0
	 *
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/includes
	 */

	/**
	 * Fired during plugin activation.
	 *
	 * This class defines all code necessary to run during the plugin's activation.
	 *
	 * @since      2.4.0
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/includes
	 * @author     André Winther Olsen <ao@morningtrain.dk>
	 */
	if(!class_exists('Woo_Advanced_QTY_Activator')) {
		class Woo_Advanced_QTY_Activator {

			/**
			 * Activation
			 *
			 * @since 2.4.0
			 */
			public static function activate() {
				//Check if user is allowed to do this
				if(!current_user_can('activate_plugins')) {
					return;
				}

				$plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
				check_admin_referer("activate-plugin_{$plugin}");
			}
		}
	}
