<?php
	/**
	 * Fired during plugin deactivation
	 *
	 * @link       http://morningtrain.dk
	 * @since      1.0.0
	 *
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/includes
	 */

	/**
	 * Fired during plugin deactivation.
	 *
	 * This class defines all code necessary to run during the plugin's deactivation.
	 *
	 * @since      1.0.0
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/includes
	 * @author     Martin Schadegg Rasch Jensen <ms@morningtrain.dk>
	 */
	if(!class_exists('Woo_Advanced_QTY_Deactivator')) {
		class Woo_Advanced_QTY_Deactivator {

			/**
			 * Deactivation
			 *
			 * @since 1.0.0
			 * @since 2.4.0
			 */
			public static function deactivate() {
				//Check if user is allowed to do this
				if(!current_user_can('activate_plugins')) {
					return;
				}

				$plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
				check_admin_referer("deactivate-plugin_{$plugin}");
			}
		}
	}
