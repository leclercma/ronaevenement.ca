<?php
	/**
	 * The admin-specific functionality of the plugin.
	 *
	 * @link       http://morningtrain.dk
	 * @since      1.0.0
	 *
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/admin
	 */

	/**
	 * The admin-specific functionality of the plugin.
	 *
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/admin
	 * @author     Martin Schadegg Rasch Jensen <ms@morningtrain.dk>
	 */
	if(!class_exists('Woo_Advanced_Qty_Admin')) {
		class Woo_Advanced_QTY_Admin {

			/**
			 * The ID of this plugin.
			 *
			 * @since    1.0.0
			 * @access   private
			 * @var      string $plugin_name The ID of this plugin.
			 */
			private $plugin_name;

			/**
			 * The version of this plugin.
			 *
			 * @since    1.0.0
			 * @access   private
			 * @var      string $version The current version of this plugin.
			 */
			private $version;

			/**
			 * Initialize the class and set its properties.
			 *
			 * @since 1.0.0
			 *
			 * @param string $plugin_name The name of this plugin.
			 * @param string $version     The version of this plugin.
			 */
			public function __construct($plugin_name, $version) {
				$this->plugin_name = $plugin_name;
				$this->version = $version;
			}

			/**
			 * Add tab for WooCommerce Advanced Quantity settings under products
			 *
			 * @since 1.4.0
			 *
			 * @param $product_tabs
			 *
			 * @return mixed
			 */
			public function add_product_tab($product_tabs) {
				// If product is grouped, don't show produc tab,
				// since it won't be used anyway.
				if(wc_get_product()->is_type('grouped')) return $product_tabs;

				// Add product tab
				$product_tabs['woo_advanced_qty'] = array(
					'label'  => __('WooCommerce Advanced Quantity', $this->plugin_name),
					'target' => 'woo_advanced_qty',
					'class'  => array(),
				);

				return $product_tabs;
			}

			/**
			 * Save global options for the whole store
			 *
			 * @since 2.1.2
			 */
			public function modify_global_settings() {
				if(isset($_POST) && isset($_POST['woo-advanced-qty-step-intervals-unformated'])) {
					if(!empty($_POST['woo-advanced-qty-step-intervals-unformated']) && $_POST['woo-advanced-qty-step-intervals-unformated'] > 0) {
						\update_option('woo-advanced-qty-step-intervals-unformated', $_POST['woo-advanced-qty-step-intervals-unformated']);
						\update_option('woo-advanced-qty-step-intervals', $this->make_step_interval($_POST['woo-advanced-qty-step-intervals-unformated']));
					} else {
						\update_option('woo-advanced-qty-step-intervals-unformated', '');
						\update_option('woo-advanced-qty-step-intervals', '');
					}
				}
			}

			/**
			 * Making the array into the right format
			 *
			 * @since 2.0.0
			 *
			 * @param $intervals
			 *
			 * @return array
			 */
			private function make_step_interval($intervals) {
				$string = trim($intervals);
				$arr = explode('|', $string);

				$step_intervals = array();
				foreach($arr AS $item) {
					$step_intervals[] = explode(',', $item);
				}

				return $step_intervals;
			}

			/**
			 * Show options on every single product
			 *
			 * @since 1.0.0
			 */
			public function product_options() {
				global $thepostid;

				echo '<div id="woo_advanced_qty" class="panel woocommerce_options_panel">';

				echo '<div class="options_group">';

				// Minimum
				woocommerce_wp_text_input(array(
						'id'                => 'advanced-qty-min',
						'label'             => __('Minimum', $this->plugin_name),
						'desc_tip'          => 'true',
						'description'       => __('The minimum quantity a customer has to order of this product.', $this->plugin_name),
						'value'             => get_post_meta($thepostid, '_advanced-qty-min', true),
						'type'              => 'number',
						'custom_attributes' => array('step' => '0.01', 'min' => '0'),
					)
				);

				// Maximum
				woocommerce_wp_text_input(array(
						'id'                => 'advanced-qty-max',
						'label'             => __('Maximum', $this->plugin_name),
						'desc_tip'          => 'true',
						'description'       => __('The maximum quantity a customer can add to same order of this product.', $this->plugin_name),
						'value'             => get_post_meta($thepostid, '_advanced-qty-max', true),
						'type'              => 'number',
						'custom_attributes' => array('step' => '0.01', 'min' => '0'),
					)
				);

				// Step
				woocommerce_wp_text_input(array(
						'id'                => 'advanced-qty-step',
						'label'             => __('Step', $this->plugin_name),
						'desc_tip'          => 'true',
						'description'       => __('The step between allowed quantities.', $this->plugin_name),
						'value'             => get_post_meta($thepostid, '_advanced-qty-step', true),
						'type'              => 'number',
						'custom_attributes' => array('step' => '0.01', 'min' => '0'),
					)
				);

				// Step intervals
				woocommerce_wp_text_input(array(
						'id'                => 'advanced-qty-step-intervals',
						'label'             => __('Step intervals', $this->plugin_name),
						'desc_tip'          => 'true',
						'placeholder'       => __('Example: 0,10,5|10,100,10', $this->plugin_name),
						'custom_attributes' => array('value' => '12'),
						'description'       => __('The step between allowed quantities intervals (example: 0,10,5|10,100,10) which means from 0 to 10 it will increase by 5 and 10 to 100 will increase with 10', $this->plugin_name),
						'value'             => $this->convert_to_right_value(get_post_meta($thepostid, '_advanced-qty-step-intervals', true)),
						'type'              => 'text',
					)
				);

				// Standard value
				woocommerce_wp_text_input(array(
						'id'                => 'advanced-qty-value',
						'label'             => __('Standard value', $this->plugin_name),
						'desc_tip'          => 'true',
						'description'       => __('The standard value the quantity fields shows.', $this->plugin_name),
						'value'             => get_post_meta($thepostid, '_advanced-qty-value', true),
						'type'              => 'number',
						'custom_attributes' => array('step' => '0.01', 'min' => '0'),
					)
				);

				echo '</div>';

				echo '<div class="options_group">';

				// Price suffix
				woocommerce_wp_text_input(array(
						'id'          => 'advanced-qty-price-suffix',
						'label'       => __('Price suffix', $this->plugin_name),
						'desc_tip'    => 'true',
						'description' => __('Text to add after price (example: pr. 100g)', $this->plugin_name),
						'value'       => get_post_meta($thepostid, '_advanced-qty-price-suffix', true),
						'type'        => 'text',
					)
				);

				echo '</div>';

				echo '<div class="options_group">';

				// Quantity suffix
				woocommerce_wp_text_input(array(
						'id'          => 'advanced-qty-quantity-suffix',
						'label'       => __('Quantity suffix', $this->plugin_name),
						'desc_tip'    => 'true',
						'description' => __('Text to add after chosen quantity (example: x 100 g)', $this->plugin_name),
						'value'       => get_post_meta($thepostid, '_advanced-qty-quantity-suffix', true),
						'type'        => 'text',
					)
				);

				echo '</div>';

				echo '<div class="options_group">';

				// Input picker
				woocommerce_wp_select(array(
						'id'          => 'advanced-qty-input-picker',
						'label'       => __('Input picker', $this->plugin_name),
						'desc_tip'    => 'true',
						'description' => __('The input picker that controls how the quantity is increased/decreased', $this->plugin_name),
						'value'       => get_post_meta($thepostid, '_advanced-qty-input-picker', true),
						'type'        => 'text',
						'options'     => $this->inputTypes(),
					)
				);

				echo '</div>';

				echo '</div>';
			}

			/**
			 * Making the array into a readable string so we can display it
			 *
			 * @since 2.0.0
			 *
			 * @param $intervals
			 *
			 * @return string
			 */
			private function convert_to_right_value($intervals) {
				if(empty($intervals)) return '';

				$string = '';
				$count = count($intervals);
				$i = 1;

				foreach($intervals AS $item) {
					$string .= $item[0] . ',' . $item[1] . ',' . $item[2];

					// Separations the values
					if($i != $count) {
						$string .= '|';
					}

					$i++;
				}

				return $string;
			}

			/**
			 * Gets the input types
			 *
			 * @since 2.3.0
			 *
			 * @param bool $leave_out_global
			 *
			 * @return array
			 */
			public function inputTypes($leave_out_global = false) {
				$inputs = array(
					'global-input'     => __('Follow global setting', $this->plugin_name),
					'default-input'    => __('Default', $this->plugin_name),
					'slider-input'     => __('Slider', $this->plugin_name),
					'plus-minus-input' => __('+/-', $this->plugin_name),
					'drop-down-input'  => __('Dropdown', $this->plugin_name),
				);

				if($leave_out_global) {
					array_shift($inputs);
				}

				return $inputs;
			}

			/**
			 * Display plugin requirement errors
			 *
			 * @since 2.4.0
			 */
			public function checkAdminNotices() {
				// Get plugin requirements
				$errors = $this->checkPluginRequirements();

				// Remove empty values
				$errors = array_filter($errors);

				// Check if no errors
				if(empty($errors)) return;

				// Get plugin data, defined in woo-advanced-qty.php
				$plugin = get_plugin_data(plugin_dir_path(__FILE__) . '../woo-advanced-qty.php');

				// Print error
				printf('<div class="notice notice-error"><p><strong><i>%1$s</i> %2$s</strong></p></div>', $plugin['Name'], join('<p></p>', $errors));
			}

			/**
			 * Check the plugin requirements
			 *
			 * @since 2.4.0
			 *
			 * @return array
			 */
			public function checkPluginRequirements() {
				$errors[] = array();
				// Check if WooCommerce is installed and activated
				if(!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
					$errors['woocommerce_error'] = sprintf(__('requires WooCommerce to be installed and activated. You can download %s here.', 'woo-advanced-qty'), '<a href="https://woocommerce.com/">WooCommerce</a>');
				}

				return $errors;
			}

			/**
			 * Displays the settings link on the plugin page
			 *
			 * @since 2.4.0
			 *
			 * @param $links
			 *
			 * @return mixed
			 */
			public function displayPluginSettingsLink($links) {
				// Compose settings link
				$settings_link = '<a href="' . admin_url('admin.php?page=wc-settings&tab=advanced_quantity') . '">' . __('Settings', 'woo-advanced-qty') . '</a>';
				// Display settings link before deactivate
				array_unshift($links, $settings_link);

				return $links;
			}

			/**
			 * Show options on every single product category when adding new
			 *
			 * @since 1.1.0
			 *
			 * @param $tag
			 */
			public function category_options_add($tag) {
				$inputs = $this->inputTypes();

				echo '<div><h2>' . __('Advanced Quantity', $this->plugin_name) . '</h2></div>';

				echo '<div class="form-field"><label for="advanced-qty-min">' . __('Minimum', $this->plugin_name), '</label><input type="number" name="advanced-qty-min" id="advanced-qty-min" value="" step="0.01" min="0"></div>';

				echo '<div class="form-field"><label for="advanced-qty-max">' . __('Maximum', $this->plugin_name), '</label><input type="number" name="advanced-qty-max" id="advanced-qty-max" value="" step="0.01" min="0"></div>';

				echo '<div class="form-field"><label for="advanced-qty-step">' . __('Step', $this->plugin_name), '</label><input type="number" name="advanced-qty-step" id="advanced-qty-step" value="" step="0.01" min="0"></div>';

				echo '<div class="form-field"><label for="advanced-qty-step-intervals">' . __('Step intervals', $this->plugin_name), '</label><input type="text" name="advanced-qty-step-intervals" id="advanced-qty-step-intervals" placeholder="' . __('Example: 0,10,5|10,100,10', $this->plugin_name) . '" value=""></div>';

				echo '<div class="form-field"><label for="advanced-qty-value">' . __('Standard value', $this->plugin_name), '</label><input type="number" name="advanced-qty-value" id="advanced-qty-value" value="" step="0.01" min="0"></div>';

				echo '<div class="form-field"><label for="advanced-qty-price-suffix">' . __('Price suffix', $this->plugin_name), '</label><input type="text" name="advanced-qty-price-suffix" id="advanced-qty-price-suffix" value=""></div>';

				echo '<div class="form-field"><label for="advanced-qty-quantity-suffix">' . __('Quantity suffix', $this->plugin_name), '</label><input type="text" name="advanced-qty-quantity-suffix" id="advanced-qty-quantity-suffix" value=""></div>';

				echo '<div class="form-field"><label for="advanced-qty-input-picker">' . __('Input picker', $this->plugin_name), '</label><select name="advanced-qty-input-picker" id="advanced-qty-input-picker">';
				foreach($inputs as $type => $input) {
					echo '<option value="' . $type . '">' . $input . '</option>';
				}
				echo '</select></div>';
			}

			/**
			 * Show options on every single product category when editing
			 *
			 * @since 1.1.0
			 *
			 * @param $tag
			 */
			public function category_options_edit($tag) {
				$type = get_option('product-category-advanced-qty-input-picker-' . $tag->term_id);
				$inputs = $this->inputTypes();

				echo '<tr><th><h2>' . __('Advanced Quantity', $this->plugin_name) . '</h2></th></tr>';

				echo '<tr class="form-field"><th scope="row" valign="top"><label for="advanced-qty-min">' . __('Minimum', $this->plugin_name), '</label></th><td><input type="number" name="advanced-qty-min" id="advanced-qty-min" value="' . get_option('product-category-advanced-qty-min-' . $tag->term_id) . '" step="0.01" min="0"></td></tr>';

				echo '<tr class="form-field"><th scope="row" valign="top"><label for="advanced-qty-max">' . __('Maximum', $this->plugin_name), '</label></th><td><input type="number" name="advanced-qty-max" id="advanced-qty-max" value="' . get_option('product-category-advanced-qty-max-' . $tag->term_id) . '" step="0.01" min="0"></td></tr>';

				echo '<tr class="form-field"><th scope="row" valign="top"><label for="advanced-qty-step">' . __('Step', $this->plugin_name), '</label></th><td><input type="number" name="advanced-qty-step" id="advanced-qty-step" value="' . get_option('product-category-advanced-qty-step-' . $tag->term_id) . '" step="0.01" min="0"></td></tr>';

				echo '<tr class="form-field"><th scope="row" valign="top"><label for="advanced-qty-step-intervals">' . __('Step intervals', $this->plugin_name), '</label></th><td><input type="text" name="advanced-qty-step-intervals" id="advanced-qty-step-intervals" value="' . $this->convert_to_right_value(get_option('product-category-advanced-qty-step-intervals-' . $tag->term_id)) . '" placeholder="' . __('Example: 0,10,5|10,100,10', $this->plugin_name) . '"></td></tr>';

				echo '<tr class="form-field"><th scope="row" valign="top"><label for="advanced-qty-value">' . __('Standard value', $this->plugin_name), '</label></th><td><input type="number" name="advanced-qty-value" id="advanced-qty-value" value="' . get_option('product-category-advanced-qty-value-' . $tag->term_id) . '" step="0.01" min="0"></td></tr>';

				echo '<tr class="form-field"><th scope="row" valign="top"><label for="advanced-price-suffix">' . __('Price suffix', $this->plugin_name), '</label></th><td><input type="text" name="advanced-qty-price-suffix" id="advanced-qty-price-suffix" value="' . get_option('product-category-advanced-qty-price-suffix-' . $tag->term_id) . '"></td></tr>';

				echo '<tr class="form-field"><th scope="row" valign="top"><label for="advanced-quantity-suffix">' . __('Quantity suffix', $this->plugin_name), '</label></th><td><input type="text" name="advanced-qty-quantity-suffix" id="advanced-qty-quantity-suffix" value="' . get_option('product-category-advanced-qty-quantity-suffix-' . $tag->term_id) . '"></td></tr>';

				echo '<tr class="form-field"><th scope="row" valign="top"><label for="advanced-input-picker">' . __('Input picker', $this->plugin_name), '</label></th><td><select name="advanced-qty-input-picker" id="advanced-qty-input-picker">';
				foreach($inputs as $val => $input) {
					echo '<option value="' . $val . '" ' . ($type == $val ? 'selected' : '') . '>' . $input . '</option>';
				}
				echo '</select></td></tr>';
			}

			/**
			 * Adds a new settings tab in WooCommerce settings
			 *
			 * @since 2.3.0
			 *
			 * @param $setting_tabs
			 *
			 * @return mixed
			 */
			public function addSettingsTab($setting_tabs) {
				$setting_tabs['advanced_quantity'] = __('Advanced Quantity', $this->plugin_name);

				return $setting_tabs;
			}

			/**
			 * Gets the settings and displays it
			 * in the settings tab
			 *
			 * @since 2.3.0
			 */
			public function getSettings() {
				woocommerce_admin_fields($this->generalSettings());
			}

			/**
			 * General/Global plugin settings
			 *
			 * @since 2.3.0
			 *
			 * @return array
			 */
			public function generalSettings() {
				$product_settings = array(
					'section_title'           => array(
						'title' => __('Advanced Quantity', $this->plugin_name),
						'type'  => 'title',
						'id'    => 'advanced_qty_options_product',
						'desc'  => __('Set to "0" to deactivate the global values.', $this->plugin_name),
					),
					'setting_minimum'         => array(
						'name'              => __('Minimum', $this->plugin_name),
						'desc'              => __('This controls the minimum a customer can add to an order of products', $this->plugin_name),
						'desc_tip'          => true,
						'id'                => 'woo-advanced-qty-min',
						'type'              => 'number',
						'css'               => 'min-width: 100px;',
						'custom_attributes' => array('step' => '0.01', 'min' => '0'),
						'default'           => 0,
					),
					'setting_maximum'         => array(
						'name'              => __('Maximum', $this->plugin_name),
						'desc'              => __('This controls the maximum a customer can add to an order of a product.', $this->plugin_name),
						'desc_tip'          => true,
						'id'                => 'woo-advanced-qty-max',
						'type'              => 'number',
						'css'               => 'min-width: 100px;',
						'custom_attributes' => array('step' => '0.01', 'min' => '0'),
						'default'           => 0,
					),
					'setting_step'            => array(
						'name'              => __('Step', $this->plugin_name),
						'desc'              => __('This controls the way that quantity increments.', $this->plugin_name),
						'desc_tip'          => true,
						'id'                => 'woo-advanced-qty-step',
						'type'              => 'number',
						'css'               => 'min-width: 100px;',
						'custom_attributes' => array('step' => '0.01', 'min' => '0'),
						'default'           => 0,
					),
					'setting_step_intervals'  => array(
						'name'        => __('Step intervals', $this->plugin_name),
						'desc'        => __('Example: 0,10,5|10,100,10 which means from 0 to 10 it will increase by 5 and 10 to 100 will increase with 10.', $this->plugin_name),
						'desc_tip'    => true,
						'id'          => 'woo-advanced-qty-step-intervals-unformated',
						'type'        => 'text',
						'placeholder' => 'Example: 0,10,5|10,100,10',
						'css'         => 'min-width: 100px;',
						'default'     => 0,
					),
					'setting_standard_value'  => array(
						'name'              => __('Standard value', $this->plugin_name),
						'desc'              => __('This controls the standard value for the quantity fields.', $this->plugin_name),
						'desc_tip'          => true,
						'id'                => 'woo-advanced-qty-value',
						'type'              => 'number',
						'css'               => 'min-width: 100px;',
						'custom_attributes' => array('step' => '0.01', 'min' => '0'),
						'default'           => 0,
					),
					'setting_price_suffix'    => array(
						'name'     => __('Price suffix', $this->plugin_name),
						'desc'     => __('This controls the standard price suffix for the price suffix fields.', $this->plugin_name),
						'desc_tip' => true,
						'id'       => 'woo-advanced-qty-price-suffix',
						'type'     => 'text',
						'css'      => 'min-width: 100px;',
						'default'  => 0,
					),
					'setting_quantity_suffix' => array(
						'name'     => __('Quantity suffix', $this->plugin_name),
						'desc'     => __('This controls the standard value for the quantity suffix fields.', $this->plugin_name),
						'desc_tip' => true,
						'id'       => 'woo-advanced-qty-quantity-suffix',
						'type'     => 'text',
						'css'      => 'min-width: 100px;',
						'default'  => 0,
					),
					'setting_input_picker'    => array(
						'name'     => __('Input picker', $this->plugin_name),
						'desc'     => __('This controls the input type for the quantity field.', $this->plugin_name),
						'desc_tip' => true,
						'id'       => 'woo-advanced-qty-input-picker',
						'type'     => 'select',
						'options'  => $this->inputTypes(true),
						'css'      => 'min-width: 100px;',
					),
					array(
						'type' => 'sectionend',
						'id'   => 'advanced_qty_options_product',
					),
				);

				$cart_settings = array(
					'cart_section_title'                 => array(
						'title' => __('Cart', $this->plugin_name),
						'type'  => 'title',
						'id'    => 'advanced_qty_options_cart',
						'desc'  => __('Settings for the cart page', $this->plugin_name),
					),
					'setting_price_suffix_cart'          => array(
						'name' => __('Price suffix', $this->plugin_name),
						'desc' => __('Show the price suffix on the cart page.', $this->plugin_name),
						'id'   => 'woo-advanced-qty-show-price-suffix-on-cart-page',
						'type' => 'checkbox',
						'css'  => 'min-width: 100px;',
					),
					'setting_quantity_suffix_cart'       => array(
						'name' => __('Quantity suffix', $this->plugin_name),
						'desc' => __('Show the quantity suffix on the cart page.', $this->plugin_name),
						'id'   => 'woo-advanced-qty-show-quantity-suffix-on-cart-page',
						'type' => 'checkbox',
						'css'  => 'min-width: 100px;',
					),
					'setting_input_picker_cart'          => array(
						'name'     => __('Input picker', $this->plugin_name),
						'desc'     => __('This controls the input type for the quantity field.', $this->plugin_name),
						'desc_tip' => true,
						'id'       => 'woo-advanced-qty-cart-page-input-picker',
						'type'     => 'select',
						'options'  => $this->inputTypes(true),
						'css'      => 'min-width: 100px',
					),
					'setting_auto_cart_refresh'          => array(
						'name'          => __('Automatic cart refresh', $this->plugin_name),
						'desc'          => __('Automatically update cart on quantity change', $this->plugin_name),
						'id'            => 'woo-advanced-qty-triggers-auto-cart-refresh',
						'type'          => 'checkbox',
						'css'           => 'min-width: 100px',
						'checkboxgroup' => 'start',
					),
					'setting_hide_update_button'         => array(
						'desc'          => __('Hide update button', $this->plugin_name),
						'desc_tip'      => __('Only a good idea if Automatic cart refresh is activated or something else handles cart refresh', $this->plugin_name),
						'id'            => 'woo-advanced-qty-hide-update-button',
						'type'          => 'checkbox',
						'css'           => 'min-width: 100px',
						'checkboxgroup' => 'end',
					),
					array(
					'type' => 'sectionend',
					'id'   => 'advanced_qty_options_cart',
					),
				);

				$misc_settings = array(
					'misc_section_title'                 => array(
						'title' => __('Misc', $this->plugin_name),
						'type'  => 'title',
						'id'    => 'advanced_qty_options_misc',
						'desc'  => __('Miscellaneous settings', $this->plugin_name),
					),
					'setting_thousands_separator' => array(
						'name'     => __('Thousands separator', $this->plugin_name),
						'desc'     => __('Separate thousands on the Slider type', $this->plugin_name),
						'desc_tip' => __('Example: 1.000, 10.000, 100.000', $this->plugin_name),
						'id'       => 'woo-advanced-qty-slider-number-format',
						'type'     => 'checkbox',
						'css'      => 'min-width: 100px',
					),
					'setting_force_mobile_dropdown' => array(
						'name'     => __('Dropdown on mobile', $this->plugin_name),
						'desc'     => __('Force dropdown input for mobile, to avoid invalid quantity selection', $this->plugin_name),
						'id'       => 'woo-advanced-qty-force-dropdown-mobile',
						'type'     => 'checkbox',
						'css'      => 'min-width: 100px',
					),
					array(
						'type' => 'sectionend',
						'id'   => 'advanced_qty_options_misc',
					),
				);

				return array_merge($product_settings, $cart_settings, $misc_settings);
			}

			/**
			 * Updates the settings
			 *
			 * @since 2.3.0
			 */
			public function updateSettings() {
				woocommerce_update_options($this->generalSettings());
			}

			/**
			 * Save product options when saving post
			 *
			 * @since 1.0.0
			 *
			 * @param $post_id
			 * @param $post
			 */
			public function save_product_options($post_id, $post) {
				if(is_ajax()) return;

				// $post_id and $post are required
				if(empty($post_id) || empty($post) || isset($_POST['action']) && $_POST['action'] == 'inline-save' || isset($_GET['woocommerce_bulk_edit']) && $_GET['woocommerce_bulk_edit'] == 1 || !is_admin() || isset($_POST['action']) && $_POST['action'] === 'woocommerce_do_ajax_product_import' ||
					isset($_GET['action']) && $_GET['action'] === 'process' && isset($_GET['page']) && $_GET['page'] === 'pmxi-admin-import') {
					return;
				}

				// Don't save options for revisions or autosaves
				if(defined('DOING_AUTOSAVE') || is_int(wp_is_post_revision($post)) || is_int(wp_is_post_autosave($post)) || isset($_POST['action']) && $_POST['action'] !== 'editpost') {
					return;
				}

				// Min Quantity
				if(isset($_REQUEST['advanced-qty-min']) && $_REQUEST['advanced-qty-min'] > 0) {
					update_post_meta($post_id, '_advanced-qty-min', $_REQUEST['advanced-qty-min']);
				} else {
					delete_post_meta($post_id, '_advanced-qty-min');
				}

				// Quantity Step
				if(isset($_REQUEST['advanced-qty-step']) && $_REQUEST['advanced-qty-step'] > 0) {
					update_post_meta($post_id, '_advanced-qty-step', $_REQUEST['advanced-qty-step']);
				} else {
					delete_post_meta($post_id, '_advanced-qty-step');
				}

				// Max Quantity
				if(isset($_REQUEST['advanced-qty-max']) && $_REQUEST['advanced-qty-max'] > 0) {
					update_post_meta($post_id, '_advanced-qty-max', $_REQUEST['advanced-qty-max']);
				} else {
					delete_post_meta($post_id, '_advanced-qty-max');
				}

				// Quantity Value
				if(isset($_REQUEST['advanced-qty-value']) && $_REQUEST['advanced-qty-value'] > 0) {
					update_post_meta($post_id, '_advanced-qty-value', $_REQUEST['advanced-qty-value']);
				} else {
					delete_post_meta($post_id, '_advanced-qty-value');
				}

				// Price Suffix
				if(isset($_REQUEST['advanced-qty-price-suffix']) && !empty($_REQUEST['advanced-qty-price-suffix'])) {
					update_post_meta($post_id, '_advanced-qty-price-suffix', $_REQUEST['advanced-qty-price-suffix']);
				} else {
					delete_post_meta($post_id, '_advanced-qty-price-suffix');
				}

				// Quantity Suffix
				if(isset($_REQUEST['advanced-qty-quantity-suffix']) && !empty($_REQUEST['advanced-qty-quantity-suffix'])) {
					update_post_meta($post_id, '_advanced-qty-quantity-suffix', $_REQUEST['advanced-qty-quantity-suffix']);
				} else {
					delete_post_meta($post_id, '_advanced-qty-quantity-suffix');
				}

				// Quantity Input Picker
				if(isset($_REQUEST['advanced-qty-input-picker']) && !empty($_REQUEST['advanced-qty-input-picker']) && $_REQUEST['advanced-qty-input-picker'] != 'default-input') {
					update_post_meta($post_id, '_advanced-qty-input-picker', $_REQUEST['advanced-qty-input-picker']);
				} else {
					delete_post_meta($post_id, '_advanced-qty-input-picker');
				}

				// Step Intervals
				if(isset($_REQUEST['advanced-qty-step-intervals']) && !empty($_REQUEST['advanced-qty-step-intervals'])) {
					$step_intervals = $this->make_step_interval($_REQUEST['advanced-qty-step-intervals']);
					update_post_meta($post_id, '_advanced-qty-step-intervals', $step_intervals);
				} else {
					delete_post_meta($post_id, '_advanced-qty-step-intervals');
				}

				// Individually Variation
				if(isset($_REQUEST['advanced-qty-individually-variation']) && !empty($_REQUEST['advanced-qty-individually-variation'])) {
					update_post_meta($post_id, '_advanced-qty-individually-variation', 1);
				} else {
					delete_post_meta($post_id, '_advanced-qty-individually-variation');
				}
			}

			/**
			 * Save category options when saving category
			 *
			 * @since 1.1.0
			 *
			 * @param $term_id
			 * @param $tt_id
			 */
			public function save_category_options($term_id, $tt_id) {
				// $term_id and $tt_id are required
				if(empty($term_id) || empty($tt_id) || isset($_POST['action']) && $_POST['action'] == 'inline-save-tax') {
					return;
				}

				// Min Quantity
				if(isset($_REQUEST['advanced-qty-min']) && $_REQUEST['advanced-qty-min'] > 0) {
					update_option('product-category-advanced-qty-min-' . $term_id, $_REQUEST['advanced-qty-min']);
				} else {
					delete_option('product-category-advanced-qty-min-' . $term_id);
				}

				// Quantity Step
				if(isset($_REQUEST['advanced-qty-step']) && $_REQUEST['advanced-qty-step'] > 0) {
					update_option('product-category-advanced-qty-step-' . $term_id, $_REQUEST['advanced-qty-step']);
				} else {
					delete_option('product-category-advanced-qty-step-' . $term_id);
				}

				// Max Quantity
				if(isset($_REQUEST['advanced-qty-step-intervals']) && $_REQUEST['advanced-qty-step-intervals'] > 0) {
					$step_intervals = $this->make_step_interval($_REQUEST['advanced-qty-step-intervals']);
					update_option('product-category-advanced-qty-step-intervals-' . $term_id, $step_intervals);
				} else {
					delete_option('product-category-advanced-qty-step-intervals-' . $term_id);
				}

				// Quantity Max
				if(isset($_REQUEST['advanced-qty-max']) && $_REQUEST['advanced-qty-max'] > 0) {
					update_option('product-category-advanced-qty-max-' . $term_id, $_REQUEST['advanced-qty-max']);
				} else {
					delete_option('product-category-advanced-qty-max-' . $term_id);
				}

				// Quantity Value
				if(isset($_REQUEST['advanced-qty-value']) && $_REQUEST['advanced-qty-value'] > 0) {
					update_option('product-category-advanced-qty-value-' . $term_id, $_REQUEST['advanced-qty-value']);
				} else {
					delete_option('product-category-advanced-qty-value-' . $term_id);
				}

				// Price Suffix
				if(isset($_REQUEST['advanced-qty-price-suffix']) && !empty($_REQUEST['advanced-qty-price-suffix'])) {
					update_option('product-category-advanced-qty-price-suffix-' . $term_id, $_REQUEST['advanced-qty-price-suffix']);
				} else {
					delete_option('product-category-advanced-qty-price-suffix-' . $term_id);
				}

				// Quantity Suffix
				if(isset($_REQUEST['advanced-qty-quantity-suffix']) && !empty($_REQUEST['advanced-qty-quantity-suffix'])) {
					update_option('product-category-advanced-qty-quantity-suffix-' . $term_id, $_REQUEST['advanced-qty-quantity-suffix']);
				} else {
					delete_option('product-category-advanced-qty-quantity-suffix-' . $term_id);
				}

				// Quantity Input Picker
				if(isset($_REQUEST['advanced-qty-input-picker']) && !empty($_REQUEST['advanced-qty-input-picker'])) {
					update_option('product-category-advanced-qty-input-picker-' . $term_id, $_REQUEST['advanced-qty-input-picker']);
				} else {
					delete_option('product-category-advanced-qty-input-picker-' . $term_id);
				}
			}

			/**
			 * Show on inventory product
			 *
			 * @since 2.0.0
			 */
			public function variations_options() {
				global $thepostid;

				echo '<div class="options_group">';

				$checked = get_post_meta($thepostid, '_advanced-qty-individually-variation', true);

				if($checked == 1) {
					woocommerce_wp_checkbox(array(
						'id'                => 'advanced-qty-individually-variation',
						'label'             => __('Individually Variations', $this->plugin_name),
						'desc_tip'          => 'true',
						'description'       => __('Make it able to buy stock quantity for each variation.', $this->plugin_name),
						'type'              => 'checkbox',
						'custom_attributes' => array(
							'checked' => 'checked',
						),
					));
				} else {
					woocommerce_wp_checkbox(array(
						'id'          => 'advanced-qty-individually-variation',
						'label'       => __('Individually Variations', $this->plugin_name),
						'desc_tip'    => 'true',
						'description' => __('Make it able to buy stock quantity for each variation.', $this->plugin_name),
						'type'        => 'checkbox',
					));
				}

				echo '</div>';
			}
		}
	}
