��          �   %   �      p     q     �     �     �      �     �  �        �     �     �     �     �     �  +   �  B     Z   W  F   �  9   �  Q   3  =   �  +   �  C   �  -   3  $   a  <   �     �  /   �  �    (   �  5   �  ,   �     !	  %   ?	  !   e	  �   �	  &   $
     K
     S
     [
     z
     �
  8   �
  L   �
  �     %   �  3   �  �   �  3   x  '   �  \   �  (   1     Z  ]   y     �  A   �                                                                              	      
                                       Advanced Quantity Advanced quantity maximum Advanced quantity minimum Advanced quantity price suffix Advanced quantity standard value Advanced quantity step An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a> An unknown error occurred Maximum Minimum Price Suffix Standard value Step Text to add after price (example: pr. 100g) The maximum quantity a customer can add to same order of products. The maximum quantity a customer can add to same order of products. Type "0" to deactivate. The maximum quantity a customer can add to same order of this product. The minimum quantity a customer has to order of products. The minimum quantity a customer has to order of products. Type "0" to deactivate. The minimum quantity a customer has to order of this product. The standard value for the quantity fields. The standard value for the quantity fields. Type "0" to deactivate. The standard value the quantity fields shows. The step between allowed quantities. The step between allowed quantities. Type "0" to deactivate. WooCommerce Advanced Quantity You did not add a correct quantity to the cart. Project-Id-Version: 
POT-Creation-Date: 2016-03-03 07:01+0100
PO-Revision-Date: 2016-03-03 07:42+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Erweiterte Konfiguration Anzahl/Schritte Erweiterte Konfiguration maximale Anzahl der Produkte Erweiterte Konfiguration Mindestbestellmenge Erweiterte Menge Preis suffix Erweiterte Konfiguration Standardwert Erweiterte Konfiguration Schritte Ein unerwarteter HTTP Fehler trat während der API Abfrage auf.</p> <p><a href="?" onclick="document.location.reload(); return false;">Nochmal versuchen</a> Ein unbekannter Fehler ist aufgetreten Maximum Minimum Zusätzlicher Anhang zum Preis Standard-Wert Schritt Text nach Preis hinzugefügt werden (Beispiel: PR. 100g) Maximale Anzahl pro Produkt und pro Bestellung die ein Kunden eingeben darf. Maximale Anzahl pro Produkt und pro Bestellung die ein Kunden eingeben darf. Geben Sie "0" ein um diese Funktion zu deaktivieren. Das Maximum für dieses Produktpaket. Mindestbestellmenge pro Produkt und pro Bestellung: Mindestbestellmenge pro Produkt und pro Bestellung die ein Kunden eingeben darf. Geben Sie "0" ein um diese Funktion zu deaktivieren. Mindestbestellmenge pro Produkt und pro Bestellung: Der Standardwert für die Mengenfelder. Der Standardwert für die Mengenfelder. Geben Sie "0" ein um diese Funktion zu deaktivieren. Der Standardwert der Mengenfelder zeigt. Die Schritte die erlaubt sind. Der Schritt zwischen zulässigen Mengen. Geben Sie "0" ein um diese Funktion zu deaktivieren. Mindestbestellmenge Sie haben nicht die richtige Menge in den Warenkorb hinzugefügt. 