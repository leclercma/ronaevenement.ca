��          �   %   �      `     a     s     �     �      �     �  �   �     �     �     �     �     �     �  +   �  B     Z   G  F   �  9   �  Q   #  =   u  +   �  C   �  -   #  $   Q  <   v  /   �  �  �     �     �     �     �     �     	  �   *	     �	     �	     �	  
   �	     �	     
  7   
  L   ?
  k   �
  E   �
  3   >  R   r  8   �      �  J     3   j     �  J   �  ;                                                                                 	      
                                        Advanced Quantity Advanced quantity maximum Advanced quantity minimum Advanced quantity price suffix Advanced quantity standard value Advanced quantity step An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a> An unknown error occurred Maximum Minimum Price Suffix Standard value Step Text to add after price (example: pr. 100g) The maximum quantity a customer can add to same order of products. The maximum quantity a customer can add to same order of products. Type "0" to deactivate. The maximum quantity a customer can add to same order of this product. The minimum quantity a customer has to order of products. The minimum quantity a customer has to order of products. Type "0" to deactivate. The minimum quantity a customer has to order of this product. The standard value for the quantity fields. The standard value for the quantity fields. Type "0" to deactivate. The standard value the quantity fields shows. The step between allowed quantities. The step between allowed quantities. Type "0" to deactivate. You did not add a correct quantity to the cart. Project-Id-Version: WooCommerce Advanced Quantity
POT-Creation-Date: 2016-05-17 15:04+0200
PO-Revision-Date: 2016-05-17 15:05+0200
Last-Translator: 
Language-Team: 
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Avanceret antal Avanceret maksimal antal Avanceret minimum antal Avanceret prissuffix Avanceret standard antal Avanceret step mellem antal Der opstod en uventet HTTP-fejl under API-anmodningen. </p><p> <a href=“?” onclick=“document.location.reload(); return false;”>Prøv igen</a> En ukendt fejl opstod Maksimum Minimum Prissuffix Standard værdi Step Tekst, som skal vises efter prisen (eksempel: pr. 100g) Det makasimale antal, som en kunde kan købe i den samme ordre af produkter. Det makasimale antal, som en kunde kan købe i den samme ordre af produkter. Indtast "0" for at deaktivere. Det maksimale antal en kunde kan købe i samme odre af dette produkt. Det mindste antal en kunde skal købe af produkter. Det mindste antal en kunde skal købe af produkter. Indtast "0" for at deaktivere. Det mindste antal, en kunde skal købe af dette produkt. Standard antal for antal felter. The standard value for the quantity fields. Indtast "0" for at deaktivere. Det antal, som feltet antal skal vise fra start af. Steps mellem tilladte antal. Det antal der er mellem hvert tilladt step. Indtast "0" for at deaktivere. Du har ikke tilføje en korrekt mængde til indkøbskurven. 