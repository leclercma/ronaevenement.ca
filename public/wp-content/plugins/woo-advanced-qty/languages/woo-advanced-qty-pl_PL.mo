��          �      ,      �     �     �     �      �     �  B     Z   J  F   �  9   �  Q   &  =   x  +   �  C   �  -   &  $   T  <   y  �  �     z     �     �     �     �  <   �  i     <   �  .   �  [   �  .   I  '   x  d   �  '   	  +   -	  j   Y	                                                   	              
             Advanced Quantity Advanced quantity maximum Advanced quantity minimum Advanced quantity standard value Advanced quantity step The maximum quantity a customer can add to same order of products. The maximum quantity a customer can add to same order of products. Type "0" to deactivate. The maximum quantity a customer can add to same order of this product. The minimum quantity a customer has to order of products. The minimum quantity a customer has to order of products. Type "0" to deactivate. The minimum quantity a customer has to order of this product. The standard value for the quantity fields. The standard value for the quantity fields. Type "0" to deactivate. The standard value the quantity fields shows. The step between allowed quantities. The step between allowed quantities. Type "0" to deactivate. Project-Id-Version: Qty
POT-Creation-Date: 2015-08-19 11:20+0200
PO-Revision-Date: 2015-08-19 15:40+0100
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-Basepath: ../admin
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
 Zaawansowane ilości Ilość maksymalna Ilość minimalna Ilość domyślna Krok zmiany ilości Maksymalna ilość jaką klient może zamówić jednorazowo. Maksymalna ilość jaką klient może zamówić jednorazowo. Wpisz "0" żeby wyłączyć to ograniczenie. Maksymalna ilość jaką klient może zamówić jednorazowo. Minimalna ilość jaką klient musi zamówić. Minimalna ilość jaką klient musi zamówić. Wpisz "0" żeby wyłączyć to ograniczenie. Minimalna ilość jaką klient musi zamówić. Domyślna wartoś dla pola z ilością. Domyślna wartoś dla pola z ilością. Wpisz "0" jeżeli nie chcesz ustawiać wartości domyślnej. Domyślna wartoś dla pola z ilością. Krok zwiększenia lub zmniejszenia ilości. Krok zwiększenia lub zmniejszenia ilości. Wpisz "0" jeżeli nie chcesz zmieniać ustawienia domyślnego. 