var $j = jQuery.noConflict();

$j(document).ready(function() {
    
    var max_quantity = parseFloat($j('.woo-qty-amount').attr("max-item-order"));
    var min_quantity = parseFloat($j('.quantity input[name=quantity]').attr('min'));

    var in_basket = $j('.woo-qty-amount').attr("in-basket");
    in_basket = in_basket.split(',');
    
    var active_variation = $j('.variation_id').val();

    if(active_variation && active_variation != 0) {
        for(var i = 0; i < in_basket.length; i += 2) {
            if(in_basket[i] == active_variation) {
                var item = i + 1;
                $j('.quantity input[name=quantity]').attr('value', in_basket[item]).change();
            }
        }
    }


    $j('.variation_id').change(function() {
        active_variation = $j(this).val();


        if(active_variation) {

            if($j.inArray(active_variation, in_basket) < 0) {
                $j('.quantity input[name=quantity]').attr('max', max_quantity).change();
                $j('.quantity input[name=quantity]').attr('value',min_quantity).change();
            } else {
                for(var i = 0; i < in_basket.length; i++) {
                    if(in_basket[i] == active_variation) {
                        var item = i + 1;
                        $j('.quantity input[name=quantity]').attr('value', in_basket[item]).change();
                    }
                    i++;
                }
            }

        }
    });


});
