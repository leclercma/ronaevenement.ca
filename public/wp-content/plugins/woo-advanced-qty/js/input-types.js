var $j = jQuery.noConflict();

$j(document).ready(function () {
	plus_and_minus();
	init_sliders();
});

if (woo_advanced_qty['is_on_cart'] == 1) {
	$j(document).ajaxComplete(function (event, xhr, settings) {
		if (settings['url'] == woo_advanced_qty['url_to_cart']) {
			plus_and_minus();
			init_sliders();
		}
	});
}

function plus_and_minus() {

	$j(document).find('.plus-minus-button').each(function () {

		var plus_minus_button = $j(this);
		var step = parseFloat($j(this).find('#qty').attr('step'));
		var min_value = parseFloat($j(this).find('#qty').attr('min'));
		var max_value = parseFloat($j(this).find('#qty').attr('max'));
		var step_intervals = $j(this).find('#qty').attr('steps-interval');

		var product_id = $j(this).find('#qty').attr('product_id');
		var add_to_cart = $j('[data-product_id=' + product_id + ']');

		var variations_product_id = $j('.woo-qty-amount').attr('product-id');

		step_intervals = step_intervals.split(',');

		if (step_intervals.length > 2 && !$j(".variations").html()) {
			var current_value = parseFloat($j('.input-start-input').attr('value'));
			if (current_value) {
				$j(plus_minus_button).find('#qty').val(current_value).change();
			}
		}

		var decimals;

		// Checks to see how many decimals needed to be added if step is int no decimals is added
		decimals = determines_how_many_decimals(step);

		// MINUS
		$j(this).find('.woo-advanced-minus').click(function () {

			var value = parseFloat($j(plus_minus_button).find('#qty').val());
			var changed = false;

			if (step_intervals.length > 2) {
				for (var i = 0; i < step_intervals.length; i++) {
					var min_step_interval = parseFloat(step_intervals[i]);
					var max_step_interval = parseFloat(step_intervals[i + 1]);
					var step_interval_value = parseFloat(step_intervals[i + 2]);

					if (value >= min_step_interval && value <= max_step_interval) {
						value -= step_interval_value;

						decimals = determines_how_many_decimals(step_interval_value);

						if (value >= min_value) {
							$j(plus_minus_button).find('#qty').val(value.toFixed(decimals)).change();

							$j(add_to_cart).attr('data-quantity', value.toFixed(decimals)).change();
						} else {
							if (value >= min_value) {
								$j(plus_minus_button).find('#qty').val(value.toFixed(decimals)).change();
							}
						}

						changed = true;
						remove_diabled();
						break;
					}

					i += 2;
				}
			}

			if (value > min_value && changed == false && !variations_product_id) {
				value -= step;
				decimals = determines_how_many_decimals(step);
				$j(plus_minus_button).find('#qty').val(value.toFixed(decimals)).change();

				$j(add_to_cart).attr('data-quantity', value.toFixed(decimals)).change();
				remove_diabled();
			}
		});

		// PLUS
		$j(this).find('.woo-advanced-plus').click(function () {

			var value = parseFloat($j(plus_minus_button).find('#qty').val());
			var changed = false;

			// This is used on the varations product
			max_value = parseFloat($j(plus_minus_button).find('#qty').attr('max'));

			if (step_intervals.length > 0) {
				for (var i = 0; i < step_intervals.length; i++) {
					var min_step_interval = parseFloat(step_intervals[i]);
					var max_step_interval = parseFloat(step_intervals[i + 1]);
					var step_interval_value = parseFloat(step_intervals[i + 2]);

					if (value >= min_step_interval && value < max_step_interval) {
						value += step_interval_value;

						decimals = determines_how_many_decimals(step_interval_value);

						$j(plus_minus_button).find('#qty').val(value.toFixed(decimals)).change();

						$j(add_to_cart).attr('data-quantity', value.toFixed(decimals)).change();
						changed = true;
						remove_diabled();
						break;

					}

					i += 2;
				}
			}

			if (value < max_value && changed == false || isNaN(max_value) && changed == false && !variations_product_id) {
				value += step;
				decimals = determines_how_many_decimals(step);
				$j(plus_minus_button).find('#qty').val(value.toFixed(decimals)).change();

				$j(add_to_cart).attr('data-quantity', value.toFixed(decimals)).change();
				remove_diabled();
			}

		});

	});
}

function isInt(n) {
	return n % 1 === 0;
}

function return_decimals(number) {
	return (number.toString().split('.')[1] || []).length;
}

function remove_diabled() {
	$j("input[name='update_cart']").prop('disabled', false);
}

function determines_how_many_decimals(step) {
	var decimals;

	if (isInt(step)) {
		decimals = 0;
	} else {
		if (return_decimals(step) == 2) {
			decimals = 2;
		} else {
			decimals = 1;
		}
	}

	return decimals;
}

function init_sliders() {
	$j(document).find('.slider-input').each(function () {

		var slider_input = $j(this);

		var max_item_order = parseFloat($j('.woo-qty-amount').attr('max-item-order'));
		var min_item_order = parseFloat($j('.woo-qty-amount').attr('min-item-order'));
		var variations_product_id = $j('.woo-qty-amount').attr('product-id');
		var product_id = slider_input.find('input').attr('product_id');

		var max_value = parseFloat(slider_input.find('input').attr('max-value'));
		var min_value = parseFloat(slider_input.find('input').attr('min-value'));
		var step = parseFloat(slider_input.find('input').attr('step'));

		var handle = slider_input.find("#custom-handle");
		var input = slider_input.find("input");

		var values = slider_input.find('input').attr('data-steps');
		values = values.split(",");

		if (variations_product_id == product_id) {

			var in_cart = $j('.woo-qty-amount').attr('in-basket');
			in_cart = in_cart.split(',');

			var slider_product = $j(this).find('#slider');

			// This is used if variation was chosen at load
			var variations_id = parseInt($j('.variation_id').val());

			values = create_slider_datalist(values, min_value);

			// Updates slider on new select
			$j('.variation_id').change(function () {
				var active_variation = $j(this).val();

				if (active_variation) {
					if (values.length > 2) {
						change_slider_values_intervaller(in_cart, active_variation, slider_product, handle, input, values)
					} else {
						change_slider_values(in_cart, active_variation, max_item_order, min_item_order, max_value, min_value, slider_product, handle, input)
					}
				}

			});

			// Check to see if product has intervals AND variation products
			if (values.length > 2) {

				var min_value_intervals = 0;

				if (values[0] == 0) {
					min_value_intervals = 1;
				}

				slider_product.slider({
					min: min_value_intervals,
					max: values.length - 1,
					value: 1,
					create: function () {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(values[1]);
							handle.text(formatedNo);
						} else {
							handle.text(values[1]);
						}
					},
					slide: function (event, ui) {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(values[ui.value]);
							handle.text(formatedNo);
						} else {
							handle.text(values[ui.value]);
						}
						input.val(values[ui.value]);
						remove_diabled();
					},
					stop: function (event, ui) {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(values[ui.value]);
							handle.text(formatedNo);
						} else {
							handle.text(values[ui.value]);
						}
						handle.text(values[ui.value]);
						input.val([ui.value]);
						$j(this).parent().children('.qty').trigger('change');
					}
				});

				if (variations_id != 0) {
					change_slider_values_intervaller(in_cart, variations_id, slider_product, handle, input, values)
				}

			} else {

				slider_product.slider({
					min: min_value,
					max: max_value,
					value: min_value,
					step: step,
					create: function () {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(min_value);
							handle.text(formatedNo);
						} else {
							handle.text(min_value);
						}
						var formatedNo = addCommas(min_value);
						handle.text(formatedNo);
					},
					slide: function (event, ui) {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(ui.value);
							handle.text(formatedNo);
						} else {
							handle.text(ui.value);
						}
						input.val(ui.value);
						remove_diabled();
					},
					stop: function (event, ui) {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(ui.value);
							handle.text(formatedNo);
						} else {
							handle.text(ui.value);
						}
						input.val(ui.value);
						$j(this).parent().children('.qty').trigger('change');
					}
				});

				if (variations_id) {
					change_slider_values(in_cart, variations_id, max_item_order, min_item_order, max_value, min_value, slider_product, handle, input)
				}
			}

		} else {

			var current_value = parseFloat($j('.input-start-input').attr('value'));
			var current_handle = slider_input.find('.is-not-current-handle').html();

			if (isNaN(current_value) || current_handle) {
				current_value = parseFloat($j(this).find('input').attr('value'));
			} else {
				slider_input.find('#amount').attr('value', current_value);
			}

			if (current_value < 0) {
				current_value = 1;
			}

			var add_to_cart = $j('[data-product_id=' + product_id + ']');

			var current_values_index = 1;

			// Check to see if product has intervals
			if (values.length > 1) {

				values = create_slider_datalist(values, min_value);

				for (var i = 0; i < values.length; i++) {
					if (values[i] == current_value) {
						current_values_index = i;
					}
				}

				$j(this).find('#slider').slider({
					min: 1,
					max: values.length - 1,
					value: current_values_index,
					create: function () {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(values[current_values_index]);
							handle.text(formatedNo);
						} else {
							handle.text(values[current_values_index]);
						}
					},
					slide: function (event, ui) {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(values[ui.value]);
							handle.text(formatedNo);
						} else {
							handle.text(values[ui.value]);
						}
						input.val(values[ui.value]);
						$j(add_to_cart).attr('data-quantity', values[ui.value]);
						remove_diabled();
					},
					stop: function (event, ui) {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(values[ui.value]);
							handle.text(formatedNo);
						} else {
							handle.text(values[ui.value]);
						}
						input.val(values[ui.value]);
						$j(this).parent().children('.qty').trigger('change');
					}
				});

			} else {

				$j(this).find('#slider').slider({
					min: min_value,
					max: max_value,
					value: current_value,
					step: step,
					create: function () {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(current_value);
							handle.text(formatedNo);
						} else {
							handle.text(current_value);
						}
					},
					slide: function (event, ui) {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(ui.value);
							handle.text(formatedNo);
						} else {
							handle.text(ui.value);
						}
						input.val(ui.value);
						$j(add_to_cart).attr('data-quantity', ui.value);
						remove_diabled();
					},
					stop: function (event, ui) {
						if ($j(this).find('#custom-handle').data('format') === 'yes') {
							var formatedNo = addCommas(ui.value);
							handle.text(formatedNo);
						} else {
							handle.text(ui.value);
						}
						input.val(ui.value);
						$j(this).parent().children('.qty').trigger('change');
					}
				});
			}
		}

	});
}

function addCommas(nStr) {
	nStr += '';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}

$j(document).ready(function () {
	$j('.drop-down-button').each(function () {

		var select = $j(this);
		var step_intervals = $j(this).find('#qty').attr('steps-interval');

		var product_id = $j(this).find('#qty').attr('product_id');
		var add_to_cart = $j('[data-product_id=' + product_id + ']');

		var max_item_order = parseFloat($j('.woo-qty-amount').attr('max-item-order'));
		var min_item_order = parseFloat($j('.woo-qty-amount').attr('min-item-order'));
		var step = parseFloat($j(this).find('#qty').attr('step'));

		var variations_product_id = $j('.woo-qty-amount').attr('product-id');

		// This is used if variation was chosen at load
		var variations_id = $j('.variation_id').val();

		if (step_intervals == 1 && !variations_id) {
			var current_value = parseFloat($j('.input-start-input').attr('value'));

			if (!isNaN(current_value)) {
				$j(this).find('#qty').val(current_value).change();
			}
		}

		if (variations_id) {

			var in_cart = $j('.woo-qty-amount').attr('in-basket');
			if(typeof in_cart !== 'undefined') {
				in_cart = in_cart.split(',');
			}

			for (var i = 0; i < in_cart.length; i += 2) {
				if (in_cart[i] == variations_id) {
					current_value = in_cart[i + 1];
					select.find('#qty').val(current_value).change();
				}
			}

			if (!isNaN(current_value)) {
				$j(this).find('#qty').val(current_value).change();
			}
		}

		if (product_id != variations_product_id) {

			$j(this).find('#qty').change(function () {

				var value = $j(this).val();

				$j(add_to_cart).attr('data-quantity', value).change();

				remove_diabled();
			});

		} else if (step_intervals != 1) {

			$j('.variation_id').change(function () {

				max_item_order = parseFloat($j('.woo-qty-amount').attr('max-item-order'));
				min_item_order = parseFloat($j('.woo-qty-amount').attr('min-item-order'));
				step = parseFloat(select.find('#qty').attr('step'));

				var current_max_value = max_item_order;
				var current_min_value = min_item_order;

				if (!current_min_value) {
					current_min_value = 1;
				}

				if (!current_max_value) {
					current_max_value = 100;
				}

				var variations_id = $j(this).val();

				if (variations_id) {

					var in_cart = $j('.woo-qty-amount').attr('in-basket');
					in_cart = in_cart.split(',');
					var output = [];

					for (var i = 0; i < in_cart.length; i += 2) {
						if (in_cart[i] == variations_id) {

							current_max_value -= in_cart[i + 1];

							for (var j = current_min_value; j <= current_max_value; j += step) {
								output.push('<option value="' + j + '">' + j + '</option>');
							}

							select.find('#qty').html(output.join(''));

						}
					}

					if (current_max_value == max_item_order) {

						for (var j = current_min_value; j <= current_max_value; j += step) {
							output.push('<option value="' + j + '">' + j + '</option>');
						}

						select.find('#qty').html(output.join(''));
					}
				}

			});
		} else {

			$j('.variation_id').change(function () {
				var variations_id = $j(this).val();

				if (variations_id) {

					if (!min_item_order) {
						min_item_order = 1;
					}

					var in_cart = $j('.woo-qty-amount').attr('in-basket');
					in_cart = in_cart.split(',');
					current_value = min_item_order;

					for (var i = 0; i < in_cart.length; i += 2) {
						if (in_cart[i] == variations_id) {
							current_value = in_cart[i + 1];
							select.find('#qty').val(current_value).change();
						}
					}

					if (current_value == min_item_order) {
						select.find('#qty').val(min_item_order).change();
					}
				}
			});
		}
	});
});

function create_slider_datalist(steps, min_value) {

	var new_value = min_value;
	var values = [0, min_value];

	for (var i = 0; i <= steps.length - 1; i++) {
		var step_minimum = steps[i];
		var step_maximum = steps[i + 1];
		var step_value = parseFloat(steps[i + 2]);

		var step_difference = step_maximum - step_minimum;

		var decimals = determines_how_many_decimals(step_value);

		var j = 0;

		while (j < step_difference) {
			new_value += step_value;

			if (new_value.toFixed(2) <= parseFloat(step_maximum)) {
				values.push(parseFloat(new_value.toFixed(decimals)));
			} else {
				new_value -= step_value;
			}

			j += step_value;
		}

		i += 2;
	}

	return values;
}

function change_slider_values(in_cart, variations_id, max_item_order, min_item_order, max_value, min_value, slider_product, handle, input) {

	var changed = false;

	for (var i = 0; i < in_cart.length; i += 2) {
		if (in_cart[i] == variations_id) {
			max_value = max_item_order - in_cart[i + 1];

			if (max_value == 0) {
				min_value = 0;
			}
			changed = true;
		}
	}

	if (changed == false) {
		max_value = max_item_order;
		min_value = min_item_order;
	}

	slider_product.slider('option', 'max', max_value);
	slider_product.slider('option', 'min', min_value);
	slider_product.slider('option', 'value', min_value);
	handle.text(min_value);
	input.val(min_value);

}

function change_slider_values_intervaller(in_cart, variations_id, slider_product, handle, input, values) {

	var value_in_cart = 0;

	for (var i = 0; i < in_cart.length; i += 2) {
		if (in_cart[i] == variations_id) {
			value_in_cart = parseFloat(in_cart[i + 1]);
		}
	}

	var index = values.indexOf(value_in_cart);

	console.log(values);

	if (value_in_cart != 0) {

		slider_product.slider('option', 'value', index);

		if (values.length == 0) {
			handle.text(0);
			input.val(0);
		} else {
			handle.text(values[index]);
			input.val(values[index]);
		}
	} else {
		slider_product.slider('option', 'value', 1);

		if (values.length == 0) {
			handle.text(0);
			input.val(0);
		} else {
			var item = 0;

			if (values[0] == 0) {
				item = 1;
			}

			handle.text(values[item]);
			input.val(values[item]);
		}
	}
}
