<div class="drop-down-button <?php echo is_cart() ? 'on-cart-page-drop-down' : ''; ?>">
	<div class="quantity margin-top-drop-down">
		<select id="qty" class="qty" steps-interval="<?php echo $interval; ?>" name="<?php echo esc_attr($input_name); ?>" title="<?php echo esc_attr_x('Quantity', 'Dropdown quantity input tooltip', 'woocommerce') ?>" product_id="<?php echo esc_attr($product_id); ?>" step="<?php echo esc_attr($step); ?>">
			<?php foreach($intervals as $value) : ?>
				<option value="<?php echo $value; ?>" <?php echo ( ($input_value !== 0) ? (abs(($input_value - $value) / $input_value) < 0.00001) ? 'selected' : '' : '' ); ?>><?php echo $value; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
</div>