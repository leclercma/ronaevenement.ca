<?php
	if(!defined('ABSPATH')) exit; // Exit if accessed directly

	$values = array(
		'min_value'       => $min_value,
		'max_value'       => $max_value,
		'step'            => $step,
		'input_name'      => $input_name,
		'input_value'     => $input_value,
		'pattern'         => $pattern,
		'inputmode'       => $inputmode,
		'product_id'      => $product_id,
		'has_reached_max' => isset($args['product_has_reached_max']) ? $args['product_has_reached_max'] : null
	);

	do_action('waq_get_input_type', $values, $product_id);