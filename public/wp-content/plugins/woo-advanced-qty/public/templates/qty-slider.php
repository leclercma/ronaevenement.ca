<div class="slider-input <?php echo !is_single($product_id) ? 'smaller-slider' : ''; ?> <?php echo is_cart() ? 'full-width-slider' : ''; ?>">
	<div class="quantity">
		<div id="slider">
			<div id="custom-handle" data-format="<?php echo isset($format) ? $format : ''; ?>" class="ui-slider-handle"></div>
		</div>
		<input type="hidden" id="amount" class="qty" value="<?php echo esc_attr($input_value); ?>" name="<?php echo esc_attr($input_name); ?>" title="<?php echo esc_attr_x('Quantity', 'Slider quantity input tooltip', 'woocommerce') ?>" pattern="<?php echo esc_attr($pattern); ?>" max-value="<?php echo esc_attr($max_value); ?>" inputmode="<?php echo esc_attr($inputmode); ?>" min-value="<?php echo esc_attr($min_value); ?>" step="<?php echo esc_attr($step) ?>" data-steps="<?php echo $intervals; ?>" product_id="<?php echo esc_attr($product_id); ?>">
	</div>
</div>