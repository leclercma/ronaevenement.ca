<div class="plus-minus-button">
	<div class="quantity <?php echo !is_single($product_id) ? 'smaller-minus-plus' : ''; ?> <?php echo is_cart() ? 'on-cart-plus-minus-button' : ''; ?>">
		<input type='button' value='-' class='woo-advanced-minus'/>
		<input type="text" id="qty" class="plus-minus-input qty" step="<?php echo esc_attr($step); ?>" steps-interval="<?php echo $intervals; ?>"
		       min="<?php echo esc_attr($min_value); ?>" max="<?php echo esc_attr($max_value); ?>"
		       name="<?php echo esc_attr($input_name); ?>" value="<?php echo esc_attr($input_value); ?>"
		       title="<?php echo esc_attr_x('Quantity', 'Plus/minus quantity input tooltip', 'woocommerce') ?>"
		       pattern="<?php echo esc_attr($pattern); ?>" inputmode="<?php echo esc_attr($inputmode); ?>" data-product_id="<?php echo esc_attr($product_id); ?>" product_id="<?php echo esc_attr($product_id); ?>" readonly/>
		<input type='button' value='+' class='woo-advanced-plus'/>
	</div>
</div>