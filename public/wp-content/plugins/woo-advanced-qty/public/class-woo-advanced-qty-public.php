<?php
	/**
	 * The public-facing functionality of the plugin.
	 *
	 * @link       http://morningtrain.dk
	 * @since      1.0.0
	 *
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/public
	 */

	/**
	 * The public-facing functionality of the plugin.
	 *
	 * @package    Woo_Advanced_QTY
	 * @subpackage Woo_Advanced_QTY/public
	 * @author     Martin Schadegg Rasch Jensen <ms@morningtrain.dk>
	 */
	if(!class_exists('Woo_Advanced_Qty_Public')) {
		class Woo_Advanced_QTY_Public {

			/**
			 * The ID of this plugin.
			 *
			 * @since  1.0.0
			 * @access private
			 * @var    string
			 */
			private $plugin_name;

			/**
			 * The version of this plugin.
			 *
			 * @since  1.0.0
			 * @access private
			 * @var    string
			 */
			private $version;

			/**
			 * Product post title for PayPal
			 *
			 * @since  2.1.0
			 * @access private
			 * @var    array
			 */
			private $productMetaNameForPayPal;

			/**
			 * Initialize the class and set its properties.
			 *
			 * @since 1.0.0
			 *
			 * @param string $plugin_name The name of the plugin.
			 * @param string $version     The version of this plugin.
			 */
			public function __construct($plugin_name, $version) {
				$this->plugin_name = $plugin_name;
				$this->version = $version;
			}

			/**
			 * Enqueue scripts/styles if options are set
			 *
			 * @since 2.4.0
			 */
			public function enqueueIfSet() {
				$trigger_cart_refresh = get_option('woo-advanced-qty-triggers-auto-cart-refresh');
				$hide_update_button = get_option('woo-advanced-qty-hide-update-button');

				if(isset($trigger_cart_refresh) && $trigger_cart_refresh === 'yes') {
					wp_enqueue_script('woo-advanced-qty-cart-refresh', plugins_url('woo-advanced-qty/js/woo-advanced-qty-cart-refresh.js'), array('jquery'));
				}

				if(isset($hide_update_button) && $hide_update_button === 'yes') {
					wp_enqueue_style('woo-advanced-qty-hide-update-button', plugins_url('woo-advanced-qty/css/hide-update-button.css'));
				}
			}

			/**
			 * Adds quantity suffix to order review during checkout process
			 *
			 * @since 2.2.7
			 *
			 * @param string $qty
			 * @param array  $cart_item
			 *
			 * @return string
			 */
			public function addQuantitySuffxToOrderReview($qty, $cart_item) {
				if(is_array($cart_item)) {
					$product = $cart_item['data'];

					$quantity_suffix = $this->get_option($product->get_id(), 'quantity-suffix');

					if(!empty($quantity_suffix)) {
						$qty .= ' <span class="woo-adv-qty-suffix">' . $quantity_suffix . '</span>';
					}
				}

				return $qty;
			}

			/**
			 * Get option by meta name
			 *
			 * Settings applied to products overrides General & Category settings
			 * and settings applied to categories overrides General settings
			 *
			 * @since 2.0.0
			 *
			 * @param int    $product_id ID of product.
			 * @param string $identifier Option identifier, ending of the option key.
			 * @param null   $default    Optional. Default return value.
			 *
			 * @return mixed|null|string
			 */
			public function get_option($product_id, $identifier, $default = null) {
				// If is applied on the product - then use it
				$post_setting = get_post_meta($product_id, '_advanced-qty-' . $identifier, true);
				if(!empty($post_setting)) {
					// Force mobile devices to use dropdown
					if($post_setting == 'global-input' && self::isMobileDevice()) {
						return 'drop-down-input';
					}

					if($post_setting != 'global-input') {
						return $post_setting;
					}
				}

				// If setting is applied on the category - then use it
				$terms = get_the_terms($product_id, 'product_cat');

				$term_setting = '';
				if(!empty($terms)) {
					foreach($terms as $term) {
						$term_option = get_option('product-category-advanced-qty-' . $identifier . '-' . $term->term_id);

						if(!empty($term_option) && $term_option != 'global-input') {
							$term_setting = $term_option;
						}
					}

					if(!empty($term_setting)) {
						return $term_setting;
					}
				}

				// If setting is applied on the store - then use it
				$shop_setting = get_option('woo-advanced-qty-' . $identifier);
				if(!empty($shop_setting)) {
					return $shop_setting;
				}

				// Else return empty string
				return $default;
			}

			/**
			 * Adds quantity suffix to order complete (after a order is placed) and order email
			 *
			 * @since 2.2.7
			 *
			 * @param string      $qty
			 * @param \WC_Product $item
			 *
			 * @return string
			 */
			public function addQuantitySuffxToOrderComplete($qty, $item) {
				if(is_object($item)) {
					$quantity_suffix = $this->get_option($item->get_product_id(), 'quantity-suffix');

					if(!empty($quantity_suffix)) {
						$qty .= ' <span class="woo-adv-qty-suffix woo-adv-qty-completed_order_suffix">' . $quantity_suffix . '</span>';
					}
				}

				return $qty;
			}

			/**
			 * Initialize the class and set its properties.
			 *
			 * @since 2.2.6
			 *
			 * @param $total
			 * @param $order
			 * @param $item
			 * @param $inc_tax
			 * @param $round
			 *
			 * @return float
			 */
			public function changeOrderAmountItemTotal($total, $order, $item, $inc_tax, $round) {
				if($inc_tax) {
					$total = ($item->get_total() + $item->get_total_tax());
				} else {
					$total = floatval($item->get_total());
				}

				$total = $round ? round($total, wc_get_price_decimals()) : $total;

				return $total;
			}

			/**
			 * Set information for order again to work
			 *
			 * I cannot for the life of me figure out why this needs to be here
			 * in order for Order Again to work, but it has to be. And as such,
			 * here it shall remain.
			 *
			 * @since 2.1.3
			 *
			 * @param array  $cart_item_data
			 * @param array  $item
			 * @param object $order
			 *
			 * @return array
			 */
			public function addInfoForOrderAgain($cart_item_data, $item, $order) {
//				if(!ctype_digit($item['item_meta']['_qty'])) {
//					$info_about_item = array('quantity' => $item->get_quantity(), 'is_from_order_again_function' => true);
//
//					$cart_item_data = array_merge($cart_item_data, $info_about_item);
//				}

				return $cart_item_data;
			}

			/**
			 * Fix unit price on processed orders.
			 *
			 * @since 1.0.0
			 *
			 * @param      $price
			 * @param      $order
			 * @param      $item
			 * @param bool $inc_tax
			 * @param bool $round
			 *
			 * @return float|int
			 */
			public function order_amount_item_total($price, $order, $item, $inc_tax = false, $round = true) {
				$qty = (!empty($item['qty']) && $item['qty'] != 0) ? $item['qty'] : 1;
				if($inc_tax) {
					$price = ($item['line_total'] + $item['line_tax']) / $qty;
				} else {
					$price = $item['line_total'] / $qty;
				}
				$price = $round ? round($price, 2) : $price;

				return $price;
			}

			/**
			 * Fix input quantity fields problem for variations - WooCommerce bug
			 *
			 * @since 1.2.0
			 *
			 * @param $variation_data
			 * @param $product
			 * @param $variation
			 *
			 * @return mixed
			 */
			public function available_variation($variation_data, $product, $variation) {
				$args = $this->qty_input_args(array('min_value' => 1, 'max_value' => '', 'step' => 1), $product);
				if($variation_data['is_sold_individually'] != 'yes') {
					$variation_data['min_qty'] = $args['min_value'];
					$variation_data['max_qty'] = $args['max_value'];
				}

				return $variation_data;
			}

			/**
			 * Get the input args for quantity fields.
			 *
			 * @since 1.2.0
			 *
			 * @param      $args
			 * @param      $product
			 * @param bool $update_args_cart
			 *
			 * @return array
			 */
			public function qty_input_args($args, $product, $update_args_cart = true) {
				if(!isset($args['is_composite_products'])) {
					$input_type = $this->get_option($product->get_id(), 'input-picker', 'default-input');

					if(empty($args)) {
						$args = array('min_value' => 1, 'max_value' => '', 'step' => 1);
					}

					$step_value = $this->get_value('step', $product);

					if($step_value) {
						$args['step'] = $step_value;
					} else {
						$args['step'] = 1;
					}

					$min_value = $this->get_value('min', $product);
					if($min_value) {
						$args['min_value'] = $min_value;
					}

					$max_value = $this->get_value('max', $product);
					$stock_quantity = $product->backorders_allowed() ? false : $product->get_stock_quantity();
					if(($stock_quantity && $max_value && $max_value > $stock_quantity) || ($stock_quantity && !$max_value)) {
						$max_value = $stock_quantity;
					}

					if($max_value && $max_value >= $args['min_value']) {
						$max_value = $this->check_quantity($max_value, $args['min_value'], false, $args['step']);
						$args['max_value'] = $max_value;
					} else if($max_value && $max_value < $args['min_value']) {
						$args['max_value'] = $args['min_value'];
					}

					if(!is_cart()) {
						$input_value = $this->get_value('value', $product);

						if($input_value && $input_value >= $args['min_value']) {
							if($args['max_value'] > 0 && $input_value > $args['max_value']) {
								$input_value = $args['max_value'];
							}

							$input_value = $this->check_quantity($input_value, $args['min_value'], $args['max_value'], $args['step']);
							$args['input_value'] = $input_value;
							if($args['input_value'] < 0) {
								$args['input_value'] = $this->get_value('value', $product);
							}
						} else {
							$args['input_value'] = $args['min_value'];
						}
					}

					if($update_args_cart && !is_cart() && $this->is_in_cart($product->get_id())) {
						$args['max_value'] = empty($args['max_value']) ? $args['max_value'] : $args['max_value'] - $this->get_cart_qty($product->get_id());
						$args['input_value'] -= $this->get_cart_qty($product->get_id());

						if($args['input_value'] <= 0) {
							$args['input_value'] = $args['min_value'];
						}
					}

					if($input_type == 'drop-down-input' && empty($min_value)) {
						$args['min_value'] = 0;
					}

					if(!isset($args['input_value'])) {
						$args['input_value'] = $args['min_value'];
					}

					$args['pattern'] = '[0-9]+([,.][0-9]+)?';
					$args['inputmode'] = 'numeric';
					$args['product_id'] = $product->get_id();
				}

				return $args;
			}

			/**
			 * Checks the version of WooCommerce
			 *
			 * @since 2.4.0
			 *
			 * @param string $version
			 *
			 * @return bool
			 */
			public static function wooVersionCheck($version = '3.0') {
				if(class_exists('WooCommerce')) {
					global $woocommerce;

					if(version_compare($woocommerce->version, $version, ">=")) return true;
				}

				return false;
			}

			/**
			 * Find the value for a setting
			 *
			 * @since 1.2.0
			 *
			 * @param $type
			 * @param $product
			 *
			 * @return bool|int|mixed
			 */
			public function get_value($type, $product) {
				if(self::wooVersionCheck()) {
					if($product->get_parent_id() != 0) {
						$id = $product->get_parent_id();
					} else {
						$id = $product->get_id();
					}
				} else {
					if($product->id != 0) {
						$id = $product->id;
					} else {
						$id = $product->variation_id;
					}
				}

				// If setting is applied on the product then use it
				$qty = get_post_meta($id, '_advanced-qty-' . $type, true);

				if($qty > 0) return $qty;

				// If setting is applied on the category then use it
				$terms = get_the_terms($id, 'product_cat');
				if(!empty($terms)) {
					foreach($terms as $term) {
						$qty_term = $this->get_term($type, $term);

						if($qty_term > 0) {
							$qty = $qty_term;
						}
					}

					if($qty > 0) return $qty;
				}

				// If setting is applied on the store then use it
				$qty = get_option('woo-advanced-qty-' . $type);

				if($qty > 0)  return $qty;

				// Else return FALSE
				return false;
			}

			/**
			 * Find the term quantity
			 *
			 * @since 1.2.0
			 *
			 * @param $type
			 * @param $term
			 *
			 * @return int|mixed
			 */
			private function get_term($type, $term) {
				$qty = get_option('product-category-advanced-qty-' . $type . '-' . $term->term_id);

				if($qty > 0) {
					return $qty;
				} else if($term->parent > 0) {
					$term_parent = get_term($term->parent, 'product_cat');

					return $this->get_term($type, $term_parent);
				}

				return 0;
			}

			/**
			 * Check if quantity belongs to the right pattern
			 *
			 * @since 1.2.0
			 *
			 * @param     $quantity
			 * @param int $min
			 * @param int $max
			 * @param int $step
			 *
			 * @return float|int
			 */
			public function check_quantity($quantity, $min = 1, $max = 0, $step = 1) {
				$quantity = floatval($quantity);
				$min = floatval($min);
				$max = floatval($max);
				$step = floatval($step);

				if($quantity <= $min) {
					return $min;
				}

				if($max && $quantity > $max) {
					$quantity = $max;
				}

				$diff = fmod(($quantity - $min), $step);
				$diff = round($diff, 2);

				if($diff > 0.01 && $diff != $step && $quantity < $max) {
					$quantity += ($step - $diff);

					if($quantity > $max) {
						$quantity -= ($step - $diff) - $diff;
					}
				} else if($diff > 0.01 && $diff != $step && $quantity >= $max) {
					$quantity -= $diff;
				}

				return $quantity;
			}

			/**
			 * Check if the product is already in the cart
			 *
			 * @since 1.5.0
			 *
			 * @param $product_id
			 *
			 * @return bool
			 */
			public function is_in_cart($product_id) {
				if($this->get_cart_qty($product_id) > 0) return true;

				return false;
			}

			/**
			 * Get qty in cart
			 *
			 * @since 1.5.0
			 *
			 * @param $product_id
			 *
			 * @return mixed
			 */
			public function get_cart_qty($product_id) {
				global $woocommerce;

				$cart = $woocommerce->cart->cart_contents;

				if(is_array($cart) && !empty($cart)) {
					foreach($cart as $cart_item_key => $item) {
						$item_product_id = $item['data']->get_id();

						if($product_id == $item_product_id) {
							return $item['quantity'];
						}
					}
				}
			}

			/**
			 * After quantity update
			 *
			 * @since 1.2.0
			 *
			 * @param $cart_item_key
			 * @param $quantity
			 * @param $old_quantity
			 */
			public function after_quantity_update($cart_item_key, $quantity, $old_quantity) {
				global $woocommerce;

				$product = $woocommerce->cart->get_cart_item($cart_item_key);

				$input_type = $this->get_option($product['product_id'], 'input-picker', 'default-input');

				$args = $this->qty_input_args(array('min_value' => 1, 'max_value' => '', 'step' => 1), $product['data'], false);

				if(empty($quantity_new)) {
					$quantity_new = $this->check_quantity($quantity, $args['min_value'], $args['max_value'], $args['step']);
				}

				$intervals = $this->get_option($product['product_id'], 'step-intervals');

				if($input_type != 'default-input' && !$this->check_is_ajax() && !empty($intervals)) {
					$quantity_new = $quantity - $old_quantity;
					$woocommerce->cart->set_quantity($cart_item_key, 0, false);
					$woocommerce->cart->add_to_cart($product['product_id'], $quantity_new, $product['variation_id'], $product['variation']);

					return;
				}

				if($this->check_quantity_intervals($product['product_id'], $quantity)) return;

				if($quantity_new != $quantity) {
					$woocommerce->cart->set_quantity($cart_item_key, $quantity_new, false);
				}
			}

			/**
			 * Check if function is called with AJAX
			 *
			 * @since 2.0.0
			 */
			private function check_is_ajax() {
				return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
			}

			/**
			 * Check if quantity is in quantity intervals
			 *
			 * @since 2.0.0
			 *
			 * @param int       $product_id
			 * @param int|float $quantity
			 *
			 * @return bool
			 */
			public function check_quantity_intervals($product_id, $quantity) {
				$intervals = $this->get_option($product_id, 'step-intervals');
				$allowed = false;

				if(empty($intervals)) return false;

				foreach($intervals AS $interval) {
					$step_minimum = $interval[0];
					$step_maximum = $interval[1];
					$step_interval_value = $interval[2];

					for($i = $step_minimum; $i <= $step_maximum; $i += $step_interval_value) {
						if(abs(($i - $quantity) / $quantity) < 0.00001) {
							$allowed = true;
						}
					}
				}

				return $allowed;
			}

			/**
			 * Checks if user is mobile and if setting has been set
			 *
			 * @return bool
			 */
			public static function isMobileDevice() {
				return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]) && get_option('woo-advanced-qty-force-dropdown-mobile');
			}

			/**
			 * Show quantity suffix and the right input field on the cart page
			 *
			 * @since 2.2.2
			 *
			 * @param $product_quantity
			 * @param $cart_item_key
			 * @param $cart_item
			 *
			 * @return string
			 */
			public function cartItemQuantity($product_quantity, $cart_item_key, $cart_item = false) {
				if(!$cart_item) {
					global $woocommerce;

					$cart_items = $woocommerce->cart->get_cart();

					if(!empty($cart_items) && array_key_exists($cart_item_key, $cart_items)) {
						$cart_item = $cart_items[$cart_item_key];
					}
				}

				$product_id = $cart_item['product_id'];

				//Option: use selected input picker on cart page
				$cart_input_type = $this->get_option($product_id, 'cart-page-input-picker');

				if(!empty($cart_input_type) && $cart_input_type !== 'default-input') {
					$args['intervals'] = $this->get_option($product_id, 'step-intervals');
					$args['intervals'] = $this->convert_array_to_javascript_array($args['intervals']);

					$_product = \wc_get_product($product_id);
					$input_vals = $this->qty_input_args(array('min_value' => 1, 'max_value' => '', 'step' => 1), $_product);

					$args = array_merge($args, $input_vals);

					$args['input_name'] = 'cart[' . $cart_item_key . '][qty]';
					$args['input_value'] = $cart_item['quantity'];

					// Add input js
					$this->add_input_type_js();

					// Switch input picker
					switch($cart_input_type) {
						case 'slider-input': // Slider
							$product_quantity = $this->sliderPicker($args, null, false);
							break;
						case 'plus-minus-input': // Plus/Minus
							$product_quantity = static::getTemplate('qty-plus-minus-button', $args, false);
							break;
						case 'drop-down-input': // Dropdown
							$product_quantity = $this->dropdownPicker($_product, $product_id, false);
							break;
						default: // Default - WooCommerces' own
							break;
					}
				}

				// Add styling
				$this->add_qty_and_input_types_css();

				//Option: Show quantity suffix on cart page.
				$show = get_option('woo-advanced-qty-show-quantity-suffix-on-cart-page');

				if($show) {
					//Get quantity suffix
					$quantity_suffix = $this->get_option($product_id, 'quantity-suffix');

					//Append quantity suffix to product quantity input picker
					if(!empty($quantity_suffix)) {
						$product_quantity .= '<span class="cart-quantity-suffix">' . $quantity_suffix . '</span>';
					}
				}

				error_log(print_r($product_quantity, true));

				return $product_quantity;
			}

			/**
			 * Adds styling for quantity suffix
			 *
			 * @since 2.0.0
			 */
			private function addQuantitySuffixStyle() {
				\wp_enqueue_style('woo-advanced-qty-style', \plugins_url($this->plugin_name) . '/css/qty-suffix-input-types.css');
			}

			/**
			 * Add input types js
			 *
			 * @since 2.0.0
			 */
			private function add_input_type_js() {
				\wp_register_script('woo-advanced-qty-script', \plugins_url($this->plugin_name) . '/js/input-types.js', array('jquery', 'jquery-ui-slider'));
				\wp_localize_script('woo-advanced-qty-script', 'woo_advanced_qty', array(
					'url_to_cart' => \wc_get_cart_url(),
					'is_on_cart'  => \is_cart(),
				));
				\wp_enqueue_script('woo-advanced-qty-script');
			}

			/**
			 * Convert php array to javascript array
			 *
			 * @since 2.0.0
			 *
			 * @param $arr
			 *
			 * @return string
			 */
			private function convert_array_to_javascript_array($arr) {
				if(!empty($arr)) {
					$j = 1;
					$javascript_arr = '';
					$arr_length = count($arr);

					foreach($arr AS $steps_interval) {
						for($i = 0; $i <= 2; $i++) {
							if($i == 2) {
								$javascript_arr .= trim($steps_interval[$i]);
							} else {
								$javascript_arr .= trim($steps_interval[$i] . ',');
							}
						}

						if($arr_length != $j) {
							$javascript_arr .= ',';
						}

						$j++;
					}

					return $javascript_arr;
				} else {
					return '';
				}
			}

			/**
			 * Check if thoursands separator option is enabled
			 *
			 * @since 2.3.0
			 *
			 * @return string
			 */
			private function addThousandsSeparator() {
				$slider_format = get_option('woo-advanced-qty-slider-number-format');

				if(!empty($slider_format) && $slider_format === 'yes') {
					return 'yes';
				}
			}

			/**
			 * Add custom template
			 *
			 * @since 2.0.0
			 * @since 2.4.1
			 *
			 * @param       $template_name
			 * @param array $args
			 * @param bool  $echo
			 *
			 * @return string
			 */
			public static function getTemplate($template_name, $args = array(), $echo = true) {
				$template_name = str_replace('.', '/', $template_name);

				$file = plugin_dir_path(__FILE__) . 'templates/' . $template_name . '.php';

				$html = '';

				if(file_exists($file)) {
					\extract($args);

					\ob_start();
					include($file);
					$html = \ob_get_clean();
				}

				if($echo) {
					echo $html;
				}

				return $html;
			}

			/**
			 * Generates array for our drop-down-input
			 *
			 * @since 2.0.0
			 *
			 * @param $values
			 *
			 * @return array
			 */
			private function generate_select_input($values) {
				if(is_array($values)) {
					$product = \wc_get_product($values['product_id']);
				} else if(is_object($values)) {
					$product = $values;
				}

				$stock = $product->get_stock_quantity();

				if(is_array($values)) {
					$max_value = $values['max_value'];
					$min_value = $values['min_value'];
					$step = $values['step'];
				} else {
					$max_value = $this->get_value('max', $values);
					$min_value = $this->get_value('min', $values);
					$step = $this->get_value('step', $values);
				}

				if(empty($max_value)) {
					if(empty($stock)) {
						$max_value = \apply_filters('woo_advanced_qty-standard_max_value', 100);
					} else {
						$max_value = $stock;
					}
				}

				if(empty($step)) {
					$step = 1;
				}

				$select = array();

				for($i = $min_value; $i <= $max_value; $i += $step) {
					$number = number_format($i, 2, '.', '');

					if(substr($number, -1) == 0) {
						$number = substr_replace($number, "", -1);
					}

					if(substr($number, -2) == 0) {
						$number = substr_replace($number, "", -2);
					}

					$select[] = $number;
				}

				if(!empty($stock)) {
					if(abs(($i - $max_value) / $max_value) < 0.00001) {
						$number = number_format($i, 2, '.', '');

						if(substr($number, -1) == 0) {
							$number = substr_replace($number, "", -1);
						}

						if(substr($number, -2) == 0) {
							$number = substr_replace($number, "", -2);
						}

						$select[] = $number;
					}
				}

				return $select;
			}

			/**
			 * Generates interval array for our drop-down-input
			 *
			 * @since 2.0.0
			 * @since 2.4.1
			 *
			 * @param $product_id
			 *
			 * @return array
			 */
			private function generate_select_intervals_input($product_id) {
				$intervals = $this->get_option($product_id, 'step-intervals');
				$select = array();
				$first = true;

				foreach($intervals AS $interval) {
					$step_minimum = $interval[0];
					$step_maximum = $interval[1];
					$step_interval_value = $interval[2];

					if($step_minimum < 0 || empty($step_maximum) || empty($step_interval_value)) {
						break;
					}

					for($i = $step_minimum; $i <= $step_maximum; $i += $step_interval_value) {
						if(strlen(round($i, 2) - (int) round($i, 2)) - 2 == 1) {
							$decimals = 1;
						} else if(strlen(round($i, 2) - (int) round($i, 2)) - 2 < 1) {
							$decimals = 0;
						} else {
							$decimals = 2;
						}

						if($first) {
							$select[] = number_format($i, $decimals, '.', '');
							$first = false;
						} else if($i != $step_minimum) {
							$select[] = number_format($i, $decimals, '.', '');
						}
					}
				}

				$select = $this->isGroupedProductChild($select, $product_id);

				return $select;
			}

			/**
			 * Add woo-advanced-qty-style css
			 *
			 * @since 2.0.0
			 */
			private function add_qty_and_input_types_css() {
				$this->addQuantitySuffixStyle();
				wp_enqueue_style('style_jquery_ui', plugins_url($this->plugin_name) . '/css/jquery-ui.css');
			}

			/**
			 * Show price suffix on the cart page
			 *
			 * @since 2.2.2
			 *
			 * @param $product_price
			 * @param $cart_item
			 * @param $cart_item_key
			 *
			 * @return string
			 */
			public function cartItemPrice($product_price, $cart_item, $cart_item_key) {
				$this->addQuantitySuffixStyle();
				$show = get_option('woo-advanced-qty-show-price-suffix-on-cart-page');

				if($show == 'yes') {
					$price_suffix = $this->get_option($cart_item['product_id'], 'price-suffix');

					if(!empty($price_suffix)) {
						$product_price = $product_price . '<span class="cart-price-suffix">' . $price_suffix . '</span>';
					}
				}

				return $product_price;
			}

			/**
			 * Update cart quantity
			 *
			 * @since 2.2.1
			 *
			 * @param $valid
			 * @param $cart_item_key
			 * @param $cart_item_data
			 * @param $quantity
			 *
			 * @return bool
			 */
			public function update_cart_validation($valid, $cart_item_key, $cart_item_data, $quantity) {
				if($quantity > 0) {
					$product_id = $cart_item_data['product_id'];

					// Check quantity
					$product = new WC_Product($product_id);
					$args = $this->qty_input_args(array(), $product, false);

					// Individually variations
					$input_type = $this->get_option($product_id, 'input-picker', 'default-input');
					$intervals = $this->get_option($product_id, 'step-intervals');
					$individally_varitions = $this->get_option($product_id, 'individually-variation');

					if($input_type == 'slider-input' && !empty($intervals) || $individally_varitions == 1 || $input_type == 'drop-down-input' && !empty($intervals)) {
						global $woocommerce;
						$cart_items = $woocommerce->cart->get_cart();

						foreach($cart_items as $item => $values) {
							if($values['product_id'] == $product_id && $values['variation_id'] == $cart_item_data['variation_id']) {
								if($quantity == $args['max_value'] && empty($intervals)) {
									wc_add_notice(__('You have reached the maximum.', $this->plugin_name), 'error');

									return false;
								}
							}
						}

						if(!$this->is_valid_qty($quantity, $args['min_value'], $args['max_value'], $args['step'])) {
							wc_add_notice(__('You did not add a correct quantity to the cart.', $this->plugin_name), 'error');

							return false;
						}

						if(!$this->check_quantity_intervals($product_id, $quantity) && !empty($intervals)) {
							wc_add_notice(__('You did not add a correct quantity to the cart.', $this->plugin_name), 'error');

							return false;
						}

						return $valid;
					}

					if(!empty($args['max_value']) && $quantity > $args['max_value'] && empty($intervals)) {
						wc_add_notice(__('The maximum quantity allowed to purchase for this product is', $this->plugin_name) . ' ' . $args['max_value'], 'error');

						return false;
					}

					if(!$this->is_valid_qty($quantity, $args['min_value'], $args['max_value'], $args['step'])) {
						wc_add_notice(__('You did not add a correct quantity to the cart.', $this->plugin_name), 'error');

						return false;
					}
				}

				return $valid;
			}

			/**
			 * Check if quantity belongs to the right pattern
			 *
			 * @since 1.5.0
			 *
			 * @param      $quantity
			 * @param int  $min
			 * @param bool $max
			 * @param int  $step
			 *
			 * @return bool
			 */
			public function is_valid_qty($quantity, $min = 1, $max = false, $step = 1) {
				if($quantity < $min) return false;

				if(!empty($max) && $quantity > $max) return false;

				$diff = fmod(($quantity - $min), $step);

				if($diff > 0.01 && $diff = !$step) return false;

				return true;
			}

			/**
			 * Add to cart item quantity
			 *
			 * @since 1.5.0
			 *
			 * @param $cart_item_data
			 * @param $cart_item_key
			 *
			 * @return mixed
			 */
			public function add_to_cart_item_quantity($cart_item_data, $cart_item_key) {
				$product_factory = new \WC_Product_Factory();
				$product = $product_factory->get_product($cart_item_data['product_id']);
				$args = $this->qty_input_args(array('min_value' => 1, 'max_value' => '', 'step' => 1), $product);

				// Individually variations
				$individually = $this->get_option($cart_item_data['product_id'], 'individually-variation');
				$input_type = $this->get_option($cart_item_data['product_id'], 'input-picker', 'default-input');
				$intervals = $this->get_option($cart_item_data['product_id'], 'step-intervals');

				// Variations Products
				if(!empty($cart_item_data['variation_id']) && $individually == 1 || $input_type == 'plus-minus-input' && !empty($intervals) || $input_type == 'drop-down-input' && !empty($intervals)) {
					global $woocommerce;
					$cart_items = $woocommerce->cart->get_cart();

					foreach($cart_items as $item => $values) {
						if($values['product_id'] == $cart_item_data['product_id'] && $values['variation_id'] == $cart_item_data['variation_id']) {
							$cart_item_data['quantity'] += $values['quantity'];
						}
					}

					return $cart_item_data;
				}

				if(empty($quantity_new)) {
					$quantity_new = $this->check_quantity($cart_item_data['quantity'], $args['min_value'], $args['max_value'], $args['step']);
				}

				if($quantity_new != $cart_item_data['quantity']) {
					$cart_item_data['quantity'] = $quantity_new;
				}

				return $cart_item_data;
			}

			/**
			 * Check if min quantity equels maximum quantity and set the product to be sold individually
			 *
			 * @since 1.3.0
			 *
			 * @param $return
			 * @param $product
			 *
			 * @return mixed
			 */
			public function is_sold_individually($return, $product) {
				$args = $this->qty_input_args(array('min_value' => 1, 'max_value' => '', 'step' => 1), $product);

				if($args['min_value'] == $args['max_value']) {
					// return TRUE;
				}

				return $return;
			}

			/**
			 * Set the correct quantity for individually products
			 *
			 * @since 1.3.0
			 *
			 * @param $qty_individually
			 * @param $qty
			 * @param $product_id
			 * @param $variation_id
			 * @param $cart_item_data
			 *
			 * @return mixed
			 */
			public function add_to_cart_sold_individually_quantity($qty_individually, $qty, $product_id, $variation_id, $cart_item_data) {
				$product = new WC_Product($product_id);
				$args = $this->qty_input_args(array('min_value' => 1, 'max_value' => '', 'step' => 1), $product);
				if($args['min_value'] == $args['max_value']) {
					return $args['min_value'];
				}

				return $qty_individually;
			}

			/**
			 * WooCommerce bug fix - show the right quantity in the cart for individually products
			 *
			 * @since 1.3.0
			 *
			 * @param      $qty_html
			 * @param      $cart_item_key
			 * @param null $cart_item
			 *
			 * @return string
			 */
			public function cart_item_quantity($qty_html, $cart_item_key, $cart_item = null) {
				if($cart_item == null) {
					global $woocommerce;

					$cart_item = $woocommerce->cart->get_cart_item($cart_item_key);
				}

				if(isset($cart_item['data']) && $cart_item['data']->is_sold_individually()) {
					$qty_html = $cart_item['quantity'] . '<input type="hidden" name="cart[' . $cart_item_key . '][qty]" value="' . $cart_item['quantity'] . '" />';
				}

				return $qty_html;
			}

			/**
			 * Displays items meta names
			 *
			 * @since 2.1.0
			 *
			 * @param String $output
			 * @param Object $product
			 *
			 * @return String $output
			 */
			public function orderItemsMetaDisplay($output, $product) {
				$product_id = $product->meta['_product_id'][0];
				$mainProduct = $this->getMainProduct($product_id);

				if(!empty($product->meta['_variation_id'][0])) {
					$metaKey = $product->meta['_variation_id'][0];
					$name = $mainProduct->post->post_title . ' ( ' . $output . ' )';
				} else if(!empty($output)) {
					$metaKey = $product_id;
					$metaValue = $mainProduct->post->post_title . ' ( ' . $output . ' )';

					if(strlen($metaValue) > 124) {
						$metaValue = substr($metaValue, 0, 124) . '...';
					}

					$name = $metaValue;
				} else {
					$metaKey = $product_id;
					$name = $mainProduct->post->post_title;
				}

				$this->productMetaNameForPayPal[$metaKey] = $name;

				return $output;
			}

			/**
			 * Get product by ID
			 *
			 * @since 2.1.0
			 *
			 * @param $product_id
			 *
			 * @return object
			 */
			private function getMainProduct($product_id) {
				return wc_get_product($product_id);
			}

			/**
			 * Fixed issue when quantity is lower than 1
			 *
			 * @since 2.2.0
			 *
			 * @param $subtotal
			 * @param $order
			 * @param $item
			 * @param $inc_tax
			 * @param $round
			 *
			 * @return float|int|string
			 */
			public function amountItemSubTotal($subtotal, $order, $item, $inc_tax, $round) {
				global $woocommerce;

				if(intval($woocommerce->version) >= 3) {
					if($item->get_quantity() < 1) {
						$subtotal = 0;

						if(is_callable(array($item, 'get_subtotal'))) {
							if($inc_tax) {
								$subtotal = ($item->get_subtotal() + $item->get_subtotal_tax()) / $item->get_quantity();
							} else {
								$subtotal = ($item->get_subtotal() / $item->get_quantity());
							}

							$subtotal = $round ? number_format((float) $subtotal, wc_get_price_decimals(), '.', '') : $subtotal;
						}
					}
				} else {
					if($item['item_meta']['_qty'][0] < 1) {
						if($inc_tax) {
							$price = ($item['line_subtotal'] + $item['line_subtotal_tax']) / $item['item_meta']['_qty'][0];
						} else {
							$price = ($item['line_subtotal'] / $item['item_meta']['_qty'][0]);
						}

						$subtotal = $round ? number_format((float) $price, wc_get_price_decimals(), '.', '') : $price;
					}
				}

				return $subtotal;
			}

			/**
			 * Add quantity to paypal items quantity
			 *
			 * @since 2.2.0
			 *
			 * @param $item
			 * @param $item_name
			 * @param $quantity
			 * @param $amount
			 *
			 * @return mixed
			 */
			public function paypalLineItem($item, $item_name, $quantity, $amount) {
				if(is_float($quantity)) {
					$item['quantity'] = $quantity;
				}

				return $item;
			}

			/**
			 * Fix PayPal Decimal quantity issue
			 *
			 * @since 1.3.1
			 *
			 * @param $args
			 * @param $order
			 *
			 * @return mixed
			 */
			public function paypal_args($args, $order) {
				global $woocommerce;

				$cart_items = $woocommerce->cart->get_cart();

				foreach($args as $key => $arg) {
					if(preg_match('/quantity_/', $key)) {
						$index = substr($key, 9);
						if(is_int($args['quantity_' . $index])) {
							foreach($cart_items as $item) {
								$productMetaNamePayPalKey = $this->getProductMetaKey($item);
								$productMetaNamePayPal = $this->productMetaNameForPayPal[$productMetaNamePayPalKey];
								if(!is_int($item['quantity']) &&
									$args['item_name_' . $index] == $productMetaNamePayPal &&
									(int) $item['quantity'] == $args['quantity_' . $index] &&
									(float) $item['data']->price == $args['amount_' . $index]
								) {
									$quantity_suffix = $this->get_option($item['product_id'], 'quantity-suffix');
									$args['quantity_' . $index] = $item['quantity'];
								}
							}
						}
						if(!is_int($args['quantity_' . $index])) {
							$args['amount_' . $index] = round($args['amount_' . $index] * $args['quantity_' . $index], 2);
							$args['item_name_' . $index] = $this->getNewProductName($args, $index, isset($quantity_suffix) ? $quantity_suffix : '');
							$args['quantity_' . $index] = 1;
						}
					}
				}

				return $args;
			}

			/**
			 * Get key for PayPal Array
			 *
			 * @since 2.1.0
			 *
			 * @param $item
			 *
			 * @return integer
			 */
			public function getProductMetaKey($item) {
				if(!empty($item['variation_id'])) {
					return $item['variation_id'];
				} else {
					return $item['product_id'];
				}
			}

			/**
			 * Get new product name for PayPal
			 *
			 * @since 2.1.0
			 *
			 * @param $args            array
			 * @param $index           integer
			 * @param $quantity_suffix string
			 *
			 * @return string
			 */
			public function getNewProductName($args, $index, $quantity_suffix = null) {
				if(!empty($quantity_suffix)) {
					return $args['item_name_' . $index] . ' x ' . $args['quantity_' . $index] . ' ' . $quantity_suffix;
				}

				return $args['item_name_' . $index] . ' x ' . $args['quantity_' . $index];
			}

			/**
			 * Fix problem in backend
			 *
			 * @since 1.4.1
			 *
			 * @param $step
			 * @param $product
			 *
			 * @return float
			 */
			public function order_items_quantity_step($step, $product) {
				if(is_admin()) {
					$step = 0.01;
				}

				return $step;
			}

			/**
			 * Check if add to cart quantity is valid
			 *
			 * @since 1.5.0
			 *
			 * @param      $valid
			 * @param      $product_id
			 * @param      $quantity
			 * @param int  $variation_id
			 * @param null $variations
			 * @param null $cart_item_data
			 *
			 * @return bool
			 * @throws Exception
			 */
			public function add_to_cart_qty_validation($valid, $product_id, $quantity, $variation_id = 0, $variations = null, $cart_item_data = null) {
				$product = wc_get_product($product_id);
				// Check quantity
				$args = $this->qty_input_args(array(), $product, false);

				// Individually variations
				$input_type = $this->get_option($product_id, 'input-picker', 'default-input');
				$intervals = $this->get_option($product_id, 'step-intervals');
				$individally_varitions = $this->get_option($product->get_id(), 'individually-variation');

				if(isset($cart_item_data['is_from_order_again_function']) && $cart_item_data['is_from_order_again_function']) {
					$quantity = $cart_item_data['quantity'];
				}

				if($input_type == 'slider-input' && !empty($intervals) || $individally_varitions == 1 || $input_type == 'drop-down-input' && !empty($intervals)) {
					global $woocommerce;
					$cart_items = $woocommerce->cart->get_cart();

					foreach($cart_items as $item => $values) {
						if($values['product_id'] == $product_id && $values['variation_id'] == $variation_id) {
							if($quantity == $args['max_value'] && empty($intervals)) {
								wc_add_notice(__('You have reached the maximum.', $this->plugin_name), 'error');

								return false;
							}
						}
					}

					if(!$this->is_valid_qty($quantity, $args['min_value'], $args['max_value'], $args['step'])) {
						wc_add_notice(__('You did not add a correct quantity to the cart.', $this->plugin_name), 'error');

						return false;
					}

					if(!$this->check_quantity_intervals($product_id, $quantity) && !empty($intervals)) {
						wc_add_notice(__('You did not add a correct quantity to the cart.', $this->plugin_name), 'error');

						return false;
					}

					$valid = $this->addItemToCartOnOrderAgain($cart_item_data, $product_id, $quantity, $variation_id, $variations, $valid);

					return $valid;
				}

				// Return TRUE if is in cart already
				if($this->is_in_cart($product_id)) {
					$quantity += $this->get_cart_qty($product_id);
				}

				if(!empty($args['max_value']) && $quantity > $args['max_value'] && empty($intervals)) {
					wc_add_notice(__('The maximum quantity allowed to purchase for this product is', $this->plugin_name) . ' ' . $args['max_value'], 'error');

					return false;
				}

				if(!$this->is_valid_qty($quantity, $args['min_value'], $args['max_value'], $args['step'])) {
					wc_add_notice(__('You did not add a correct quantity to the cart.', $this->plugin_name), 'error');

					return false;
				}

				$valid = $this->addItemToCartOnOrderAgain($cart_item_data, $product_id, $quantity, $variation_id, $variations, $valid);

				return $valid;
			}

			/**
			 * Adding item to cart if user is trying to order again with the order again method
			 *
			 * @since 2.1.3
			 *
			 * @param $cart_item_data
			 * @param $product_id
			 * @param $quantity
			 * @param $variation_id
			 * @param $variations
			 * @param $valid
			 *
			 * @return bool
			 * @throws Exception
			 */
			public function addItemToCartOnOrderAgain($cart_item_data, $product_id, $quantity, $variation_id, $variations, $valid) {
				if(isset($cart_item_data['is_from_order_again_function']) && $cart_item_data['is_from_order_again_function'] && $valid) {
					WC()->cart->add_to_cart($product_id, $quantity, $variation_id, $variations, $cart_item_data);

					$valid = false;
				}

				return $valid;
			}

			/**
			 * Add siffix to price
			 *
			 * @since 1.5.0
			 *
			 * @param $price_display_suffix
			 * @param $product
			 *
			 * @return string
			 */
			public function add_price_suffix($price_display_suffix, $product) {
				$suffix = $this->get_option($product->get_id(), 'price-suffix');

				if(!empty($suffix)) {
					$price_display_suffix = ' <small class="woocommerce-price-suffix">' . $suffix . '</small>';
				}

				return $price_display_suffix;
			}

			/**
			 * Change quantity for archive add to cart button
			 *
			 * @since 1.5.0
			 *
			 * @param string      $link
			 * @param \WC_Product $product
			 *
			 * @return null|string|string[]
			 */
			public function archive_add_to_cart_button($link, $product) {
				$args = $this->qty_input_args(array('min_value' => 1, 'max_value' => '', 'step' => 1), $product);

				$link = preg_replace('/data-quantity="[0-9]*"/', 'data-quantity="' . $args['input_value'] . '"', $link);

				return $link;
			}

			/**
			 * Change quantity for add to cart url
			 *
			 * @since 1.5.3
			 *
			 * @param string      $url
			 * @param \WC_Product $product
			 *
			 * @return mixed|string
			 */
			public function add_to_cart_url($url, $product) {
				if(strpos($url, 'add-to-cart=')) {
					$args = $this->qty_input_args(array('min_value' => 1, 'max_value' => '', 'step' => 1), $product);

					$url = preg_replace('/&quantity=[0-9.]*/', '', $url);

					$url .= '&quantity=' . $args['input_value'];
				}

				return $url;
			}

			/**
			 * Handles data for the drop down picker and
			 * either returns or echoes it
			 *
			 * @param array|\WC_Product $values     Array of values or \WC_Product object.
			 * @param null|int          $product_id Product id.
			 * @param bool              $echo       To echo or not to echo.
			 *
			 * @since 2.4.2
			 *
			 * @return string
			 */
			public function dropdownPicker($values, $product_id, $echo = true) {
				if(empty($values['intervals'])) {
					$values['intervals'] = $this->generate_select_input(is_object($values) ? \wc_get_product($product_id) : $values);
				} else {
					$values['intervals'] = $this->generate_select_intervals_input($product_id);
				}

				if(is_array($values['intervals'])) {
					$values['interval'] = 1;
				} else {
					$values['interval'] = 0;
				}

				return static::getTemplate('qty-drop-down', $values, $echo);
			}

			/**
			 * Handles data for the slider picker and
			 * etiehr returns or echoes it
			 *
			 * @param array|\WC_Product $values     Array of values or \WC_Product object.
			 * @param null|int          $product_id Unsued. For consistency.
			 * @param bool              $echo       To echo or not to echo.
			 *
			 * @since 2.4.2
			 *
			 * @return string
			 */
			public function sliderPicker($values, $product_id = null, $echo = true) {
				$values['format'] = $this->addThousandsSeparator();

				if(empty($values['max_value'])) {
					$values['max_value'] = 100;
				}

				return static::getTemplate('qty-slider', $values, $echo);
			}

			/**
			 * Add specific input picker
			 *
			 * @since 2.0.0
			 *
			 * @param $values
			 * @param $product_id
			 */
			public function add_specific_input_picker($values, $product_id) {
				// Get input picker type
				$type = $this->get_option($product_id, 'input-picker');

				// If no max value for product
				if(is_array($values) && empty($values['max_value'])) {
					// Get max val option
					$opt_max = $this->get_option($product_id, 'max');
					// Get stock quantity
					$stock = \wc_get_product($product_id)->get_stock_quantity();

					// Check if empty or bigger than stock
					if(empty($opt_max) || $opt_max > $stock) {
						// If yes, use stock
						$values['max_value'] = $stock;
					} else {
						// If no, use option
						$values['max_value'] = $opt_max;
					}
				}

				// Get intervals
				$values['intervals'] = $this->get_option($product_id, 'step-intervals');
				// Convert it
				$values['intervals'] = $this->convert_array_to_javascript_array($values['intervals']);
				// JS for inputs
				$this->add_input_type_js();

				switch ($type) {
					case 'slider-input': // Slider
						$this->sliderPicker($values);
						break;
					case 'plus-minus-input': // Plus/Minus
						static::getTemplate('qty-plus-minus-button', $values);
						break;
					case 'drop-down-input': // Dropdown
						$this->dropdownPicker($values, $product_id);
						break;
					default: // Default - WooCommerces own
						break;
				}
			}

			/**
			 * Checks if current qurried product post has grouped children,
			 * then prepends the intervals array with 0. Makes 0 a valid
			 * selection option
			 *
			 * @since 2.4.1
			 *
			 * @param array $select
			 * @param int   $product_id
			 *
			 * @return mixed
			 */
			private function isGroupedProductChild($select, $product_id) {
				global $wp_query;

				if(!empty($wp_query->post) && is_a($wp_query->post, 'WP_Post')) {
					$product = \wc_get_product($wp_query->post->ID);
					$grouped_children = $product->get_children();

					if(!empty($grouped_children)) {
						foreach($grouped_children as $child_id) {
							if($child_id === $product_id) {
								array_unshift($select, 0);
							}
						}
					}
				}

				return $select;
			}

			/**
			 * Add suffix between quantity and buy button
			 *
			 * @since 2.0.0
			 *
			 * @param $template_name
			 * @param $template_path
			 * @param $located
			 * @param $args
			 */
			public function add_quantity_suffix($template_name, $template_path, $located, $args) {
				if($template_name == 'global/quantity-input.php') {
					global $post;

					$suffix = $this->get_option($post->ID, 'quantity-suffix');

					if(!empty($suffix)) {
						static::getTemplate('qty-suffix', array('name' => $suffix));
					}
				}
			}

			/**
			 * Overrides WooCommerces templates
			 *
			 * @since 2.0.0
			 *
			 * @param $located
			 * @param $template_name
			 * @param $args
			 * @param $template_path
			 * @param $default_path
			 *
			 * @return string
			 */
			public function woocommerce_locate_template($located, $template_name, $args, $template_path, $default_path) {
				if($template_name == 'global/quantity-input.php' && !is_cart()) {
					$product_id = $args['product_id'];
					$type = $this->get_option($product_id, 'input-picker', 'default-input');

					if($type != 'default-input') {
						$plugin_path = untrailingslashit(plugin_dir_path(__FILE__)) . '/templates/';

						// Modification: Get the template from this plugin, if it exists
						if(file_exists($plugin_path . $template_name)) {
							$located = $plugin_path . $template_name;
						}
					}

					$this->add_qty_and_input_types_css();
				}

				// Return what we found
				return $located;
			}

			/**
			 * Generating info for variations
			 *
			 * @since 2.0.0
			 */
			public function allow_individually_on_variations_product() {
				global $woocommerce;
				global $product;

				$individally_varitions = $this->get_option($product->get_id(), 'individually-variation');
				$max_quantity = $this->get_option($product->get_id(), 'max');
				$min_quantity = $this->get_option($product->get_id(), 'min');
				$input_type = $this->get_option($product->get_id(), 'input-picker', 'default-input');

				if($individally_varitions == 1 && $input_type != 'default-input') {
					$in_basket = '';

					$items = $woocommerce->cart->get_cart();
					$count = count($items);
					$i = 1;

					foreach($items as $item => $values) {
						if($count == $i) {
							$in_basket .= '' . $values['variation_id'] . ',' . $values['quantity'];
						} else {
							$in_basket .= '' . $values['variation_id'] . ',' . $values['quantity'] . ',';
						}
						$i++;
					}

					echo '<div class="woo-qty-amount" max-item-order="' . $max_quantity . '" min-item-order="' . $min_quantity . '" in-basket="' . $in_basket . '" product-id="' . $product->get_id() . '"></div>';

					if($input_type == 'plus-minus-input') {
						$this->add_variations_plus_minus_js();
					}
				}
			}

			/**
			 * Add variations js
			 *
			 * @since 2.0.0
			 */
			private function add_variations_plus_minus_js() {
				\wp_enqueue_script('woo-advanced-qty-variation-plus-minus-button-script', \plugins_url($this->plugin_name) . '/js/variations-plus-minus-button.js');
			}

			/**
			 * Add start value to inputs
			 *
			 * @since 2.0.0
			 */
			public function add_extra_info_for_input() {
				global $woocommerce;
				global $product;

				if(empty($product)) return;

				$input_type = $this->get_option($product->get_id(), 'input-picker', 'default-input');
				$intervals = $this->get_option($product->get_id(), 'step-intervals');

				$items = $woocommerce->cart->get_cart();

				if($input_type != 'default-input' && !empty($intervals)) {
					foreach($items as $item => $values) {
						if($values['product_id'] == $product->get_id()) {
							echo '<div class="input-start-input" value="' . $values['quantity'] . '"></div>';
						}
					}
				}
			}
		}
	}





















