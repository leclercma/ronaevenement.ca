<?php
	/**
	 * @wordpress-plugin
	 * Plugin Name:          WooCommerce Advanced Quantity
	 * Plugin URI:           http://morningtrain.dk
	 * Description:          Make the most out of your WooCommerce product quantity selection. This plugin allows you to make more specific product quantity incrementation.
	 * Version:              2.4.2
	 * Author:               Morning Train Technologies ApS
	 * Author URI:           http://morningtrain.dk
	 * License:              GPL-2.0+
	 * License URI:          http://www.gnu.org/licenses/gpl-2.0.txt
	 * Text Domain:          woo-advanced-qty
	 * Domain Path:          /languages/
	 * WC tested up to:      3.5.0
	 * WC requires at least: 2.5.0
	 */

	// If this file is called directly, abort.
	if(!defined('WPINC')) die;

	/**
	 * Activation
	 *
	 * @since 2.4.0
	 */
	if(!function_exists('wooAdvancedQtyActivation')) {
		function wooAdvancedQtyActivation() {
			require_once plugin_dir_path(__FILE__) . 'includes/class-woo-advanced-qty-activator.php';
			Woo_Advanced_QTY_Activator::activate();
		}

		register_activation_hook(__FILE__, 'wooAdvancedQtyActivation');
	}

	/**
	 * Deactivation
	 *
	 * @since 1.0.0
	 * @since 2.4.0
	 */
	if(!function_exists('wooAdvancedQtyDeactivation')) {
		function wooAdvancedQtyDeactivation() {
			require_once plugin_dir_path(__FILE__) . 'includes/class-woo-advanced-qty-deactivator.php';
			Woo_Advanced_QTY_Deactivator::deactivate();
		}

		register_deactivation_hook(__FILE__, 'wooAdvancedQtyDeactivation');
	}

	/**
	 * Run plugin
	 *
	 * @since 1.0.0
	 * @since 2.4.0
	 */
	if(!function_exists('runWooAdvancedQty')) {
		require plugin_dir_path(__FILE__) . 'includes/class-woo-advanced-qty.php';

		function runWooAdvancedQty() {
			$plugin = new Woo_Advanced_QTY();
			$plugin->run();
		}

		runWooAdvancedQty();
	}




