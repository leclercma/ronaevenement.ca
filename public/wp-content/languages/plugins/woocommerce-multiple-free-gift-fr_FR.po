# Translation of Plugins - WooCommerce Multiple Free Gift - Stable (latest release) in French (France)
# This file is distributed under the same license as the Plugins - WooCommerce Multiple Free Gift - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2020-10-27 10:27:47+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/3.0.0-alpha.2\n"
"Language: fr\n"
"Project-Id-Version: Plugins - WooCommerce Multiple Free Gift - Stable (latest release)\n"

#. Author of the plugin
msgid "lilmonkee"
msgstr "lilmonkee"

#. Author URI of the plugin
msgid "http://ankitpokhrel.com"
msgstr "http://ankitpokhrel.com"

#. Description of the plugin
msgid "WooCommerce giveaway made easy."
msgstr "Cadeau WooCommerce en toute simplicité"

#. Plugin URI of the plugin
msgid "http://wordpress.org/plugins/woocommerce-multiple-free-gift"
msgstr "http://wordpress.org/plugins/woocommerce-multiple-free-gift"

#. Plugin Name of the plugin
msgid "WooCommerce Multiple Free Gift"
msgstr "WooCommerce Multiple Free Gift"